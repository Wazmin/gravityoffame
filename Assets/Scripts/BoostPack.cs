﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public struct DoseBoost
{
    public bool _isReady;
    public float _timerRecharge;
}

public class BoostPack : MonoBehaviour {

    // Events
    public delegate void MajBoostUI(int _nbBoost);
    public static event MajBoostUI OnChangeNbBoost;

    public delegate void MajFlouBoost(float tauxTempsRestant);
    public static event MajFlouBoost OnChangeTimeBoost;


    public const int NB_BOOST_MAX = 3;
    public const float TEMPS_RECHARGE = 15.0f;
    public const float TEMPS_ACTIF = 5.0f;
    public int nbBoostDispo;
    public float timerBoostActif;
    public DoseBoost[] timerRechargeBoost = new DoseBoost[NB_BOOST_MAX];
    private int currentBoost;
    bool continuer;

    // VARIABLES EXTERNES POUR LE BOOST
    public AudioSource aS;
    public AudioClip sonRechargeBoost;
    public WeaponsManager WM;
    public ShipMovement SM;
    private Player myPlayer;
    public const float SPEED_BASE = 8.0f;
    public const float SPEED_BOOSTE = 14.0f;
    public const float VITESSE_GENERALE_BASE = 1.0f;
    public const float VITESSE_RECHARGEMENT_BOOSTE = 2.0f;
    public const float VITESSE_CADENCE_TIR_BOOSTE = 1.5f;
    public bool isBoostActive;
     

	// Use this for initialization
	void Start () {
        InitBoosts();
        currentBoost = 0;
        myPlayer = GetComponent<Player>();
    }

    
	// Update is called once per frame
	void Update () {
        if (myPlayer.isDead) return;

        RefreshBoost();

        if (Input.GetButtonDown("Booster"))
        {
            UseBoost();
        }
	}

    public void InitBoosts()
    {
        nbBoostDispo = NB_BOOST_MAX;
        timerBoostActif = Time.time;
        isBoostActive = false;

       for(int i =0; i < NB_BOOST_MAX; i++)
        {
            timerRechargeBoost[i]._isReady = true;
            timerRechargeBoost[i]._timerRecharge = Time.time;
        }
           
    }

    public void UseBoost()
    {
        if (nbBoostDispo < 1) return;
        currentBoost = 2;
        continuer = true;
        while (currentBoost >= 0 && continuer) 
        {
            if (timerRechargeBoost[currentBoost]._isReady)
            {
                continuer = false;
                timerRechargeBoost[currentBoost]._isReady = false;
                timerRechargeBoost[currentBoost]._timerRecharge = Time.time;
                timerBoostActif = Time.time;
                nbBoostDispo--;

                if (!isBoostActive)
                {
                    isBoostActive = true;
                    WM.tauxVitesseRechargement = 1/VITESSE_RECHARGEMENT_BOOSTE;
                    WM.tauxVitesseCadenceTir = 1 / VITESSE_CADENCE_TIR_BOOSTE;
                    SM.speed = SPEED_BOOSTE;
                }
                if (aS.isPlaying)
                {
                    aS.Stop();
                }
                aS.Play();
            }
            currentBoost--;
        }

        if (OnChangeNbBoost != null)
            OnChangeNbBoost(nbBoostDispo);
    }

    void RefreshBoost()
    {

        if (isBoostActive)
        {
            if (OnChangeTimeBoost != null)
                OnChangeTimeBoost((Time.time - timerBoostActif) / TEMPS_ACTIF);
        }
       

        if (Time.time - timerBoostActif > TEMPS_ACTIF)
        {
            isBoostActive = false;
            WM.tauxVitesseRechargement = 1/ VITESSE_GENERALE_BASE;
            WM.tauxVitesseCadenceTir = 1/ VITESSE_GENERALE_BASE;
            SM.speed = SPEED_BASE;
            if (OnChangeTimeBoost != null)
                OnChangeTimeBoost((Time.time - timerBoostActif) / TEMPS_ACTIF);
        }

        currentBoost = 0;
        continuer = true;

       for(currentBoost=0; currentBoost< NB_BOOST_MAX; currentBoost++)
        {
            if(!timerRechargeBoost[currentBoost]._isReady 
                && Time.time - timerRechargeBoost[currentBoost]._timerRecharge > TEMPS_RECHARGE)
            {
                timerRechargeBoost[currentBoost]._isReady = true;
                aS.PlayOneShot(sonRechargeBoost,1.0f);
                nbBoostDispo++;
            }
        }
        if (OnChangeNbBoost != null)
            OnChangeNbBoost(nbBoostDispo);
    }
}
