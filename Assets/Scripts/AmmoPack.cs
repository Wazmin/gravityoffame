﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class AmmoPack : NetworkBehaviour {
    private int valeurMuntion = 60;
    public float tempsMax;
    public Vector3 axeRotation;
    float timer;
    float timerBlink;
    bool blink = false;
    public GameObject PackMuni;
    AudioSource aS;

    void Start()
    {
        timer = Time.time;
        timerBlink = Time.time;
        aS = GetComponent<AudioSource>();
    }

    void Update()
    {
        transform.Rotate(axeRotation, 120 * Time.deltaTime);

        if ((Time.time - timer) / tempsMax > 0.75f && (Time.time - timerBlink) > 0.2f)
        {
            timerBlink = Time.time;
            

            if (blink)
            {
                PackMuni.SetActive(true);
            }
            else
            {
                PackMuni.SetActive(false);
            }
            //myMaterial.material.color = tV;
            blink = !blink;
        }

        if (Time.time - timer >= tempsMax)
            Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (Time.time-timer < 0.5f) return;
        if (isServer)
        {
            if (other.transform.tag == "Player")
            {
                //Debug.Log(other.transform);
                //NetworkInstanceId id = other.transform.GetComponent<NetworkIdentity>().netId;

                other.transform.GetComponent<WeaponsManager>().nbMunitions += valeurMuntion;//AjouterMuntion(valeurMuntion);

                other.transform.GetComponent<WeaponsManager>().RpcPlayGetMuniPack();
                Destroy(this.gameObject);
            }
        }
        
    }

    public void SetRotationVector(Vector3 v)
    {
        axeRotation = v;
    }

}
