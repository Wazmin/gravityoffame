﻿using UnityEngine;
using UnityEngine.Networking;

public class Armes : NetworkBehaviour
{
    public string nom;
    public float degats = 20.0f;
    public AudioClip sonTir;
    AudioSource aS;
    public float freqTir;
    float timer;
    public GameObject bulletPrefab;
    private Player myPlayer;
    private NetworkInstanceId idPlayer;
    private Transform tCamera;

    private StatsTest StatJoueur;

    // Use this for initialization
    public override void OnStartLocalPlayer()
    {
        tCamera = transform.Find("PlayerCamera").transform;
        timer = Time.time;
        StatJoueur = GetComponent<StatsTest>();
        idPlayer = GetComponent<NetworkIdentity>().netId;
    }

    void Start()
    {
        //if (!isLocalPlayer) return;
        Cursor.visible = false;
        aS = transform.Find("PlayerCamera/PackArme").transform.GetComponent<AudioSource>();
        aS.clip = sonTir;
        myPlayer = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer) return;
        //tir de l'arme
        if (Input.GetMouseButton(0) || Input.GetAxis("Fire1") < -0.2)
        {
            if ((Time.time - timer) > freqTir)
            {
                timer = Time.time;
                //aS.clip = sonTir;
                //aS.pitch = 1.0f;
                //aS.Play();
                CmdFire1();
                

            }
        }
        if (Input.GetKey(KeyCode.M))
        {
            CmdResetPosition();
        }

    }

    // tir principal de l'arme
    [Command]
    void CmdFire1()
    {
        Transform tCam = transform.Find("PlayerCamera").transform;
  
        GameObject effetBalle = (GameObject)Instantiate(bulletPrefab, transform.position, tCam.rotation);
        NetworkServer.Spawn(effetBalle);
        Destroy(effetBalle, 2.0f);
        RpcPlaySound();
        RaycastHit hit;

        Ray myRay = new Ray(tCam.position, tCam.forward);

        if (Physics.Raycast(myRay, out hit,100))
        {
            RpcTir();
            if (hit.collider.tag == "Player" && hit.transform != transform)
            {
                RpcTirTouche();
                hit.transform.GetComponent<Player>().TakeDamages(degats, nom, myPlayer.pseudo, idPlayer);
            }

        }

    }

    // respawn en cas de pb
    [Command]
    void CmdResetPosition()
    {
        GetComponent<Player>().RpcRespawn();
    }

    [ClientRpc]
    void RpcPlaySound()
    {
        aS.PlayOneShot(sonTir, 0.5f);
    }

    [ClientRpc]
    void RpcTir()
    {
        if (!isLocalPlayer) return;
        StatJoueur.nbTir++;
    }

    [ClientRpc]
    void RpcTirTouche()
    {
        if (!isLocalPlayer) return;
        StatJoueur.nbTirTouche++;
    }

}
