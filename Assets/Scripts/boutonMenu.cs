﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class boutonMenu : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler
{
    public Image Flammes;
    public AudioClip sonBayonette;
    public void OnPointerEnter(PointerEventData eventData)
    {
        Flammes.enabled = true;
        GameObject.Find("Musique").GetComponent<AudioSource>().PlayOneShot(sonBayonette);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Flammes.enabled = false;
    }

    public void OnSelect(BaseEventData eventData)
    {
        //do your stuff when selected
    }
}