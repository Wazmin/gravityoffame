﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;



public struct LigneScore
{
    public string pseudo;
    public int kills;
    public int deaths;
    public int points;
}

public class KillReportManager : NetworkBehaviour {
    public int nbJoueursMax;
    public int nbPointsKillDuPremier;
    bool isPlaying;
    bool isFirstBlood;
    bool isFinish;
    string tmpString;
    [SyncVar]
    int numJoueur = 0;

    [SyncVar]
    public int minutesDeJeu;
    [SyncVar]
    private float timerPartie;

    SyncListString kRM = new SyncListString();
    SyncListFloat tmpAffichage = new SyncListFloat();
    Text blocText;

    public GameObject[] tLignesScores = new GameObject[4];

    //reservé affichage score
    List<NetworkInstanceId> tClassement = new List<NetworkInstanceId>();
    List<NetworkInstanceId> constListofPlayer = new List<NetworkInstanceId>();
    List<LigneScore> lignesScores = new List<LigneScore>(); 


    public float tempsSerieKill;

    // nbKill - timerDernierKill
    List<int> lPlayerStat = new List<int>();
    // netId
    List<NetworkInstanceId> lNetId = new List<NetworkInstanceId>();

    private string[] nomArmesPossibles = new string[WeaponsManager.NB_WEAPONS];

    //reduction opeClean
    private int freqClean = 0;
    float tempsAffichage = 10.0f;

    private const int TAILLE_KRM = 5;

    // Use this for initialization
    void Start () {
        if (!isServer) return;
        CmdInitList();
        InitComment();
        isPlaying = false;
        isFinish = false;
        minutesDeJeu = 5; // 5sec de decompte
        timerPartie = Time.time;
        isFirstBlood = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (isServer)
        {
            if (++freqClean > 5)
            {
                freqClean = 0;
                CmdCleanKRM();
                RpcUpdateKRM();
            }
            GameManager();
        } 
    }

    // fonction de retour de message GameManager
    private void GameManager()
    {
        foreach (NetworkInstanceId _netId in constListofPlayer)
        {
            if (!isPlaying && !isFinish)// la partie n'est pas lancé
            {
                if (numJoueur < nbJoueursMax)// en attente de joueur
                {
                    NetworkServer.FindLocalObject(_netId).GetComponent<UIManager>().RpcGameManager(0, nbJoueursMax -numJoueur);
                }
                else if (numJoueur > nbJoueursMax - 1)// tous les joueurs sont connectés
                {
                    if (Time.time - timerPartie >= 1.0f)
                    {
                        timerPartie = Time.time;
                        if (minutesDeJeu > 0)
                        {
                            NetworkServer.FindLocalObject(_netId).GetComponent<UIManager>().RpcGameManager(1, minutesDeJeu);
                            minutesDeJeu--;
                        }
                        else if (minutesDeJeu == 0)
                        {
                            NetworkServer.FindLocalObject(_netId).GetComponent<UIManager>().RpcGameManager(2, minutesDeJeu);
                            minutesDeJeu--;
                        }
                        else
                        {
                            NetworkServer.FindLocalObject(_netId).GetComponent<UIManager>().RpcGameManager(3, minutesDeJeu);
                            isPlaying = true;
                            ActiveAllPlayer(true);
                            timerPartie = Time.time;
                            minutesDeJeu = 420; //valeur total du jeu 420
                        }
                    }
                }
            }
            else if (isPlaying && !isFinish) // debut partie
            {
                if (Time.time - timerPartie >= 1.0f)
                {
                    if (minutesDeJeu > 0)
                    {
                        timerPartie = Time.time;
                        minutesDeJeu--;
                        NetworkServer.FindLocalObject(_netId).GetComponent<UIManager>().RpcGameManager(4, minutesDeJeu);
                    }
                    else
                    {
                        NetworkServer.FindLocalObject(_netId).GetComponent<UIManager>().RpcGameManager(5, minutesDeJeu);
                        ActiveAllPlayer(false);
                        isFinish = true;

                    }
                }
            }
            else if (isFinish)
            {
                if (Time.time - timerPartie >= 1.0f)
                {
                    timerPartie = Time.time;

                }

            }
        }
    }

    private void ActiveAllPlayer(bool b)
    {
        if (!isServer) return;
        foreach(NetworkInstanceId _netId in constListofPlayer)
        {
            if(!b)
            NetworkServer.FindLocalObject(_netId).GetComponent<ShipMovement>().RpcStopMovements();
            NetworkServer.FindLocalObject(_netId).GetComponent<Player>().RpcSetFrozen(!b);
            NetworkServer.FindLocalObject(_netId).GetComponent<Player>().RpcSonGrille(b);
        }
    }

    [Command]
    public void CmdInitList()
    {
        kRM.Clear();
        RpcUpdateKRM();
    }

  
    // ajout d'un kill
    //[Command]
    //public void CmdAddKRM(string victime, string tueur, string nomArme)
    //{

    //    string killReport = CreateKillComment(victime, tueur, nomArme);
    //    int nbElem = kRM.Count;
    //    float f = Time.time + tempsAffichage;

    //    if (nbElem <= 0)
    //    {
    //        kRM.Add("temp");
    //        tmpAffichage.Add(Time.time + tempsAffichage);
    //    }
    //    else if (nbElem < TAILLE_KRM)
    //    {
    //        kRM.Add("temp");
    //        tmpAffichage.Add(Time.time + tempsAffichage);
    //        decalage();
    //    }
    //    else
    //    {
    //        decalage();
    //    }

    //    kRM[0] = killReport;
    //    tmpAffichage[0] = f;
    //    RpcUpdateKRM();
    //}

    // pour essais
    public void AddKRM(string victime, string tueur, string nomArme)
    {

        string killReport = CreateKillComment(victime, tueur, nomArme);
        int nbElem = kRM.Count;
        float f = Time.time + tempsAffichage;

        if (nbElem <= 0)
        {
            kRM.Add("temp");
            tmpAffichage.Add(Time.time + tempsAffichage);
        }
        else if (nbElem < TAILLE_KRM)
        {
            kRM.Add("temp");
            tmpAffichage.Add(Time.time + tempsAffichage);
            decalage();
        }
        else
        {
            decalage();
        }

        kRM[0] = killReport;
        tmpAffichage[0] = f;
        RpcUpdateKRM();
    }

    public int RegisterPlayer(NetworkInstanceId _idPlayer)
    {
        if (!constListofPlayer.Contains(_idPlayer))
        {
            constListofPlayer.Add(_idPlayer);     
            LigneScore ls;
            ls.pseudo = ClientScene.FindLocalObject(_idPlayer).GetComponent<Player>().pseudo;
            ls.kills = 0;
            ls.deaths = 0;
            ls.points = 0;
            lignesScores.Add(ls);
            if (isServer)
            {
                NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>().RpcCouleurTrainee(numJoueur);
                numJoueur++;
                int tmpIntColor;
                foreach(NetworkInstanceId _netId in constListofPlayer)
                {
                    if(_netId != _idPlayer)
                    {
                        tmpIntColor = NetworkServer.FindLocalObject(_netId).GetComponent<Player>().numCouleur;
                        NetworkServer.FindLocalObject(_netId).GetComponent<Player>().RpcBackAllPlayerColor(tmpIntColor);
                    }

                }
                

            }
        }
        return numJoueur;

    }


    // ajout des stats score de la partie
    public void AddScore(NetworkInstanceId idVictime, NetworkInstanceId idTueur)
    {
        

        LigneScore ls;
        ls = lignesScores[constListofPlayer.IndexOf(idVictime)];
        ls.deaths++;
        lignesScores[constListofPlayer.IndexOf(idVictime)] = ls;

        if(idVictime != idTueur)
        {
            if (isFirstBlood)
            {
                isFirstBlood = false;
                if (isServer)
                {
                    foreach(NetworkInstanceId _netId in constListofPlayer)
                    {
                        NetworkServer.FindLocalObject(_netId).GetComponent<Player>().
                        RpcPlayFirstBlood();
                    }
                    
                }
            }

            ls = lignesScores[constListofPlayer.IndexOf(idTueur)];
            if (tClassement.Count > 0)
            {
                if (idVictime == tClassement[0])// on tue le joueur 1
                {
                    ls.points += nbPointsKillDuPremier;
                }
                else
                {
                    ls.points++;
                }
            }            
            else
            {
                ls.points++;
            }
            ls.kills++;
            lignesScores[constListofPlayer.IndexOf(idTueur)] = ls;
        }
        MajTableauScore();// on ordonne

    }

    //envoi de l'index du premier pour tous les clients
    void EnvoiInfoClassement()
    {
        //if (!isServer) return;
        int nbPointDuPremier;
        foreach (NetworkInstanceId _netId in constListofPlayer)
        {
            nbPointDuPremier = lignesScores[constListofPlayer.IndexOf(tClassement[0])].points -
                                    lignesScores[constListofPlayer.IndexOf(_netId)].points;

            if (isServer)
            {
                NetworkServer.FindLocalObject(_netId).GetComponent<Player>().
                  RpcInfoClassement(constListofPlayer.IndexOf(tClassement[0]), nbPointDuPremier);
            }
           
            
        }


        // envoi index premier
    }

    public void MajTableauScore()
    {
        RangerClassement();
        for(int i = 0; i<constListofPlayer.Count; i++)
        {
            SetLine(tLignesScores[i],
                lignesScores[constListofPlayer.IndexOf(tClassement[i])].pseudo,
                lignesScores[constListofPlayer.IndexOf(tClassement[i])].points,
                 lignesScores[constListofPlayer.IndexOf(tClassement[i])].kills,
                  lignesScores[constListofPlayer.IndexOf(tClassement[i])].deaths);
        }

    }

    private void SetLine(GameObject line,string pseudo,int nbPoints, int kill, int death)
    { 
        line.transform.Find("playerPseudo").GetComponent<Text>().text = pseudo;
        line.transform.Find("playerPoints").GetComponent<Text>().text = nbPoints.ToString();
        line.transform.Find("playerKills").GetComponent<Text>().text =kill.ToString();
        line.transform.Find("playerDeaths").GetComponent<Text>().text = death.ToString();
    }

    // classement par nombres de points
    private void RangerClassement()
    {
        NetworkInstanceId oldFirstPlayer;
        if (tClassement.Count>0)
        {
            oldFirstPlayer = tClassement[0];
        }
        else
        {
            oldFirstPlayer = new NetworkInstanceId(0);
        }
                 

        tClassement.Clear();
        int nbPoints = -1;
        NetworkInstanceId bestId = new NetworkInstanceId();

        foreach (NetworkInstanceId netId in constListofPlayer)
        {
                nbPoints = -1;
                foreach (NetworkInstanceId netId2 in constListofPlayer)
                {
                    if (!tClassement.Contains(netId2))
                    {
                        if (lignesScores[constListofPlayer.IndexOf(netId2)].points > nbPoints)
                        {
                            nbPoints = lignesScores[constListofPlayer.IndexOf(netId2)].points;
                            bestId = netId2;
                        }
                    }
                }
                tClassement.Add(bestId);
        }
        
       
        if( ( oldFirstPlayer == null || tClassement[0] != oldFirstPlayer) && isServer)
        {
            //lancer la voix de changement de leader
            foreach(NetworkInstanceId _netId in constListofPlayer)
            {
                NetworkServer.FindLocalObject(_netId).GetComponent<Player>().RpcPlayNewLeader(constListofPlayer.IndexOf(tClassement[0]));

            }
        }

        EnvoiInfoClassement();
    }


    private string CreateKillComment(string victime, string tueur, string nomArme)
    {
        string tmp = "objet inconnue";

        if(nomArme == nomArmesPossibles[0])
        {
            tmp = tueur + " [" + nomArme + "] " + victime ;
        }else if(nomArme == nomArmesPossibles[1]){
            tmp = tueur + " [" + nomArme + "] " + victime ;
        }
        else if (nomArme == nomArmesPossibles[2]){
            tmp = tueur + " [" + nomArme + "] " + victime ;
        }
        else if (nomArme == nomArmesPossibles[3]){
            tmp = tueur + " [" + nomArme + "] " + victime ;
        }
        else if (nomArme == nomArmesPossibles[4]){
            tmp = tueur + " [" + nomArme + "] " + victime;
        }
        else
        {
           
        }


        return tmp;

    }


    // prevue pour avoir plus de phrase toutes différentes
    private void InitComment()
    {
        nomArmesPossibles[0] = "Pistolet";
        nomArmesPossibles[1] = "Mitrailleuse";
        nomArmesPossibles[2] = "Fusil à pompe";
        nomArmesPossibles[3] = "OneShooter";
        nomArmesPossibles[4] = "Bazooka";
    }

    [ClientRpc]
    public void RpcUpdateKRM()
    {
        blocText = GameObject.Find("Canvas/KRM").GetComponent<Text>();
        string tmpText = "";
        foreach (string s in kRM)
        {
            tmpText += (s + "\n");
        }
        blocText.text = tmpText;
    }

    //decalage pour garder l'ordre temporelle
    private void decalage()
    {
        for(int i= kRM.Count - 1; i>0; i--)
        {
            kRM[i] = kRM[i - 1];
            tmpAffichage[i] = tmpAffichage[i - 1];
        }
    }

    //Clean
    [Command]
    private void CmdCleanKRM()
    {
        int indice = tmpAffichage.Count - 1;
        if (indice + 1 > 0)
        {
            if (tmpAffichage[indice] < Time.time)
            {
                kRM.RemoveAt(indice);
                tmpAffichage.RemoveAt(indice);
                CmdCleanKRM();
            }

        }
    }

    // Ajout d'un kill au report
    public void AddStatKill(NetworkInstanceId idPlayer)
    {
        if (lNetId.Contains(idPlayer))
        {
            lPlayerStat[lNetId.IndexOf(idPlayer)] += 1;
            RewardSound(lPlayerStat[lNetId.IndexOf(idPlayer)], idPlayer);
        }
        else // Ajout de l'entrée dans la liste
        {
            lNetId.Add(idPlayer);
            lPlayerStat.Add(1);
        }

        if (isServer)
        {
            NetworkServer.FindLocalObject(idPlayer).GetComponent<Player>().
                RpcUpSerieKill(lPlayerStat[lNetId.IndexOf(idPlayer)]);
        }
    }
    

    public void ResetSerieKill(NetworkInstanceId _idPlayer)
    {
        if (lNetId.Contains(_idPlayer))
        {
            lPlayerStat[lNetId.IndexOf(_idPlayer)] = 0;

            if (isServer)
            {
                NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>().
                    RpcUpSerieKill(lPlayerStat[lNetId.IndexOf(_idPlayer)]);
            }
        }
       

    }

    // test si on doit jouer un son
    private void RewardSound(int nbKill, NetworkInstanceId targetPlayer)
    {
        if (!isServer) return;
        GameObject targetedPlayer =
        NetworkServer.FindLocalObject(targetPlayer);
        targetedPlayer.GetComponent<Player>().RpcPlayPublicSound(nbKill);
    }

    public void GoToMainScreen()
    {
        NetworkManager tmpNM = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
        if (isClient)
        {
            tmpNM.StopClient();
        }
        else
        {
            tmpNM.StopHost();
        }
        
        Invoke("TempoMainScene", 1.0f);
    }

    void TempoMainScene()
    {
        Destroy(GameObject.Find("NetworkManager").GetComponent<NetworkManager>());
        SceneManager.LoadScene("menuPrincipal");
    }
}
