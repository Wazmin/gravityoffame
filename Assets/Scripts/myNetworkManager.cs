﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

using System.Collections;

public class myNetworkManager : NetworkManager {
    public Text NomGladiateur;
    public Text InputIpAdress;
    public Text InputNbJoueurs;
    public Toggle toggleManette;
    
    public void StartUpHost()
    {
        if (NomGladiateur.text.Length < 1) return;
        SetPort();
        NetworkManager.singleton.StartHost();
        SetNomGladiateur();
        GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().isHost = true;
        int nombreDeJoueur = 4;
        if (int.TryParse(InputNbJoueurs.text, out nombreDeJoueur))
        {

        }
        KillReportManager KRM = GameObject.Find("KillReportManager").GetComponent<KillReportManager>();
        KRM.nbJoueursMax = Mathf.Min(4, Mathf.Max(1, nombreDeJoueur));
    }

    public void JoinGame()
    {
        if (NomGladiateur.text.Length < 1) return;
        SetIpAdress();
        SetPort();
        NetworkManager.singleton.StartClient();
        SetNomGladiateur();
        GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().isHost = false;
    }

    void SetIpAdress()
    {
        string ipAdress = InputIpAdress.text;
        NetworkManager.singleton.networkAddress = ipAdress;
    }

    void SetPort()
    {
        NetworkManager.singleton.networkPort = 7777;
    }

    void SetNomGladiateur()
    {
        string s = NomGladiateur.text;
        if (s.Length >= 12) s = s.Substring(0, 12);
        GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().nomGladiateur = s;
        GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().controlManette = toggleManette.isOn;
    }

   

}
