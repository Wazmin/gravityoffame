﻿using System.Runtime.InteropServices;

namespace Utils
{
        public static class Import
        {
            [DllImport("XInputInterface")]
            public static extern void XInputGamePadSetState(uint playerIndex, float leftMotor, float rightMotor);
        }

}

