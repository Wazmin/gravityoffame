﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Camera3Person : NetworkBehaviour {
    public GameObject teteJoueur;
    public GameObject Camera;
    public float facteurReglage;
    float longueurOrigine;
    Vector3 vTeteCam;
    bool estRaccourcis;

    void Start()
    {
        if (!isLocalPlayer) return;
        vTeteCam = teteJoueur.transform.position - Camera.transform.position;
        longueurOrigine = vTeteCam.magnitude;
        estRaccourcis = false;
        vTeteCam.Normalize();
        Debug.Log("Start");
    }

    void Update()
    {
        if (!isLocalPlayer) return;
        if (estRaccourcis)
        {
            
            if ((teteJoueur.transform.position - transform.position).magnitude >= longueurOrigine)
            {
                estRaccourcis = false;
            }else
            {
                transform.position -= vTeteCam * facteurReglage;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enter Collide Cam");
        if (other.tag == "WallMap")
        {
            if (!isLocalPlayer) return;
            if ((teteJoueur.transform.position - Camera.transform.position).magnitude >0.5f)
            {
                Camera.transform.position += vTeteCam * facteurReglage;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (!isLocalPlayer) return;
        if (other.tag == "WallMap")
        {
            if ((teteJoueur.transform.position - Camera.transform.position).magnitude > 0.5f)
            {
                Camera.transform.position += vTeteCam * facteurReglage;
            }
        }
    }


        void OnTriggerExit(Collider other)
    {
        if (other.tag == "WallMap")
        {
            if (!isLocalPlayer) return;
            estRaccourcis = true;
        }
    }

}
