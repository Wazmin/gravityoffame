﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System;

public class Bazooka : Weapon
{
    // Events
    public delegate void MajUIMunition(bool _isCharged);
    public static event MajUIMunition OnChangeNbMun;

    public GameObject roquette;
    public float vitesseRoquette;

    // vibration manette
    private bool vibrations;
    private float timerVibrations;
    private float tempsVibrations = 0.3f;
    private float intensiteVibrations = 0.7f;

    public AudioClip sonLancementRoquette;

    public override void OnStartLocalPlayer()
    { 
        timerTirPrincipal = Time.time;
        timerZoom = Time.time;
        idPlayer = GetComponent<NetworkIdentity>().netId;
        nbChargeEnCoursTxt = GameObject.Find("Canvas/HUD/nbChargeEnCours").GetComponent<Text>();
    }


      // tir principal roquette
    public override void Tirer(string shooterName)
    {
        // regulation de la fréquence de tir
        if (Time.time - timerTirPrincipal >= frequenceTir)
        {
            if (Time.time - timerTirPrincipal >= frequenceTir && nbChargeEnCours > 0)
            {
                timerTirPrincipal = Time.time;
                
                CmdTir(shooterName, idPlayer);
                ConsommerMunition();

                if (nbChargeEnCours <= 0 && myWeaponsManager.nbMunitions <= 0)
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    vibrations = false;
                    myWeaponsManager.CmdSetWeapon(WeaponsManager.WeaponName.Pistolet);
                }
                else if (nbChargeEnCours <= 0)
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    vibrations = false;
                    TempoRecharger();
                }
            }
        } 
    }

    [Command]
    void CmdTir(string pShooterName, NetworkInstanceId _idPlayerOrigin)
    {
        RpcEffetTir(pShooterName,_idPlayerOrigin);

        GameObject rocket = (GameObject)Instantiate(NetworkManager.singleton.spawnPrefabs[2], canonFPS.position, tCam.rotation);
        rocket.transform.Rotate(Vector3.right, 90.0f);
        rocket.GetComponent<Rigidbody>().velocity = rocket.transform.up * vitesseRoquette;
        rocket.GetComponent<Roquette>().idPlayer = _idPlayerOrigin;
        rocket.GetComponent<Roquette>().nomLanceur = pShooterName;
        NetworkServer.Spawn(rocket);
        RpcAnimTir(true);
    }

    public override void SpecialUpdateAction()
    {
        if (isLocalPlayer)
        {
            if (vibrations && Time.time - timerVibrations > tempsVibrations)
            {
                Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                vibrations = false;
            }
        }
    }
    
    public override void RelacheTirer(string shooterName)
    {
        CmdAnimTir(false);
        return;
    }

    public override void TempoRecharger()
    {
        if (myWeaponsManager.nbMunitions < NB_MUNITIONS_PAR_CHARGE
               || nbChargeEnCours >= NB_CHARGES_PAR_CHARGEUR)
            return;

        if (!isReloading)
        {
            timerRechargement = Time.time;
            isReloading = true;
            aS.PlayOneShot(sonRechargement);
           CmdAnimReload(true);
        }
        else if (Time.time - timerRechargement >= tempsRechargement * myWeaponsManager.tauxVitesseRechargement)
        {
            CmdAnimReload(false);
            Recharger(); 
        }
    }

    public override void MajInfoWeapon()
    {
        if (OnChangeNbMun != null)
            OnChangeNbMun(nbChargeEnCours == 1);
        nbChargeEnCoursTxt.text = nbChargeEnCours.ToString();
    }

    [ClientRpc]
    void RpcEffetTir(string pShooterName, NetworkInstanceId idPlayerOrigin)//, uint valueIstanceIdPlayer)
    {
        aS.PlayOneShot(sonTirPrincipal, 0.5f);

        if (isLocalPlayer)
        {
            if (myWeaponsManager.myPlayer.controlManette)
            {
                Utils.Import.XInputGamePadSetState(0, intensiteVibrations, intensiteVibrations);
                timerVibrations = Time.time;
                vibrations = true;
            }

        }
    }

}
