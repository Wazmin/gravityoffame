﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;
using System.Collections;
using System;

public class Mitrailleuse : Weapon
{
    // Events
    public delegate void MajUIMunition(int nbMun);
    public static event MajUIMunition OnChangeNbMun;

    public GameObject effetTirMitrailleuse;
    public float rayonRaycast;

    // vibration manette
    private bool vibrations;
    private float timerVibrations;
    private float tempsVibrations = 0.14f;
    private float intensiteVibrationsG = 0.2f;
    private float intensiteVibrationsD = 0.2f;

    public override void OnStartLocalPlayer()
    {
        timerTirPrincipal = Time.time;
        timerZoom = Time.time;
        idPlayer = GetComponent<NetworkIdentity>().netId;
        nbChargeEnCoursTxt = GameObject.Find("Canvas/HUD/nbChargeEnCours").GetComponent<Text>();
    }


    //fonction tirer en raycast
    public override void Tirer(string shooterName)
    {
        if (!isReloading)
        {
            //regulation de la fréquence de tir
            if (Time.time - timerTirPrincipal >= frequenceTir * myWeaponsManager.tauxVitesseCadenceTir && nbChargeEnCours > 0)
            {
                timerTirPrincipal = Time.time;
                if (!jeTire)
                {
                    jeTire = true;
                    CmdAnimTir(true);
                }
                CmdTir(nbDegatPrincipal, nomArme, shooterName, idPlayer);

                ConsommerMunition();
                if (nbChargeEnCours <= 0 && myWeaponsManager.nbMunitions <= 0)
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    vibrations = false;
                    myWeaponsManager.CmdSetWeapon(WeaponsManager.WeaponName.Pistolet);
                }
                else if(nbChargeEnCours <= 0)
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    vibrations = false;
                    TempoRecharger();
                }
            }
        }
        else
        {

        }
        
           
    }

    public override void SpecialUpdateAction()
    {
        if (isLocalPlayer)
        {
            if (vibrations && Time.time - timerVibrations > tempsVibrations)
            {
                Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                vibrations = false;
            }
        }
    }

    [Command]
    void CmdTir(float pDegats, string pNomArme, string pShooterName, NetworkInstanceId _idPlayer)
    {
        //RaycastHit hit;
        bool touche = false;
        Ray myRay = new Ray(tCam.position, tCam.forward);
        // reference au joueur qui tir
        Player shooterPlayer = NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>();
        RaycastHit[] hits;
        hits = Physics.SphereCastAll(myRay, rayonRaycast, 100.0f).OrderBy(h => h.distance).ToArray();
        int i = 0;
        int maxI = hits.Count();
        while (i < maxI && !touche)
        {
            if (hits[i].collider.tag != "JambesPlayer" && hits[i].collider.tag != "TorsePlayer"
                && hits[i].collider.tag != "HeadShot" && hits[i].collider.tag != "MainCamera"
                && hits[i].collider.tag != "NotToDamage" && hits[i].collider.tag != "Player")
            {
                i = 100000; //on sort si on collide un truc qui n'est pas un joueur
            }
            else
            {
                //touche jambe ou torse sauf le sien
                if ((hits[i].collider.tag == "JambesPlayer"
                    || hits[i].collider.tag == "TorsePlayer")
                    && hits[i].collider != shooterPlayer.torseHitBox
                    && hits[i].collider != shooterPlayer.jambesHitBox)
                {
                    hits[i].transform.GetComponent<Player>().TakeDamages(pDegats, nomArme, pShooterName, _idPlayer);
                    touche = true;
                }

                if (hits[i].collider.tag == "HeadShot"
                   && hits[i].collider != shooterPlayer.headHitBox)
                {
                    hits[i].transform.GetComponent<Player>().
                        TakeDamages(pDegats * myWeaponsManager.bonusDegatHeadShot, nomArme, pShooterName, _idPlayer);
                    touche = true;
                    NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>().RpcPlayHeadShotSound();
                }
            }
            i++;
        }
        RpcEffetTir(touche);
    }

    public override void RelacheTirer(string shooterName)
    {
        if (jeTire)
        {
            jeTire = false;
            CmdAnimTir(false);
        }
        return;
    }

    public override void MajInfoWeapon()
    {
        if (OnChangeNbMun != null)
            OnChangeNbMun(nbChargeEnCours);
        nbChargeEnCoursTxt.text = nbChargeEnCours.ToString();
    }

    //instance local de l'effet
    [ClientRpc]
    void RpcEffetTir(bool touche)
    {
        GameObject effetBalle;
        if (isLocalPlayer)
        {
            effetBalle = (GameObject)Instantiate(effetTirMitrailleuse, canonFPS.position, tCam.rotation);
            if (myWeaponsManager.myPlayer.controlManette)
            {
                Utils.Import.XInputGamePadSetState(0, intensiteVibrationsG, intensiteVibrationsG);
                timerVibrations = Time.time;
                vibrations = true;
            }
            
        }
        else
        {
            effetBalle = (GameObject)Instantiate(effetTirMitrailleuse, canonExt.position, tCam.rotation);
        }
        Destroy(effetBalle, 2.0f);
        aS.PlayOneShot(sonTirPrincipal, 0.7f);
       
        if (isLocalPlayer && touche)
        {
            hitEvent();
        }
    }

}
