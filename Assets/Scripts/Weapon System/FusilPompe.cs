﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class FusilPompe : Weapon {

    // Events
    public delegate void MajUIMunition(int nbMun);
    public static event MajUIMunition OnChangeNbMun;

    public GameObject effetTirPompe;
    public AudioClip sonTirFusilPompe;
    public float porteeMaxTir;
    public float rayonSprayPompe;

    // vibration manette
    private bool vibrations;
    private float timerVibrations;
    private float tempsVibrations = 0.5f;
    private float intensiteVibrationsG = 0.4f;
    private float intensiteVibrationsD = 0.6f;

    public override void OnStartLocalPlayer()
    {
        timerTirPrincipal = Time.time;
        timerZoom = Time.time;
        idPlayer = GetComponent<NetworkIdentity>().netId;
        nbChargeEnCoursTxt = GameObject.Find("Canvas/HUD/nbChargeEnCours").GetComponent<Text>();

    }

    public override void SpecialUpdateAction()
    {
        if (isLocalPlayer)
        {
            if (vibrations && Time.time - timerVibrations > tempsVibrations)
            {
                Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                vibrations = false;
            }
        }
    }


    //fonction tirer en raycast
    public override void Tirer(string shooterName)
    {
        if (!isReloading)
        {
            //regulation de la fréquence de tir
            if (Time.time - timerTirPrincipal >= frequenceTir * myWeaponsManager.tauxVitesseCadenceTir && nbChargeEnCours > 0)
            {
                timerTirPrincipal = Time.time;
                CmdTir(shooterName, idPlayer);
                if (!jeTire)
                {
                    jeTire = true;
                    CmdAnimTir(true);
                }
                
                ConsommerMunition();
                if (nbChargeEnCours <= 0 && myWeaponsManager.nbMunitions <= 0)
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    vibrations = false;
                    myWeaponsManager.CmdSetWeapon(WeaponsManager.WeaponName.Pistolet);
                }
                else if (nbChargeEnCours <= 0)
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    vibrations = false;
                    TempoRecharger();
                }
            }

        }
        else
        {

        }


    }

    [Command]
    void CmdTir(string pShooterName, NetworkInstanceId _idPlayer)
    {
        bool touche = false;
        int layerDamage = 1 << 9; //la grosse capsule colider


        //Sphere Detection
        Collider[] hitColliders = Physics.OverlapSphere(tCam.position , porteeMaxTir, layerDamage);

        List<Collider> listColliderAdversaire = new List<Collider>();
        List<NetworkInstanceId> lNetId = new List<NetworkInstanceId>();
        NetworkInstanceId tmpId;

        Vector3 tmpV3;
        // parcours des eventuelles victimes
        foreach (Collider c in hitColliders)
        {
            tmpId = c.transform.GetComponentInParent<NetworkIdentity>().netId;
            // si c'est pas un joueur osef
            if (c.tag == "Player"
               && !lNetId.Contains(tmpId)) 
            {
                lNetId.Add(tmpId);
                tmpV3 = c.transform.position - tCam.position -tCam.forward;
                // si il est dans le rayon de spray
                if (Vector3.Angle(tCam.forward, tmpV3) <= rayonSprayPompe / 2 
                        && Vector3.Distance(c.transform.position, tCam.position) > 0.05f)
                {
                    int i = 0;
                    bool continuer = true;
                    // on cherche a les placer par ordre croissant de distance
                    while (i < listColliderAdversaire.Count && continuer)
                    {
                        if (Vector3.Distance(c.transform.position, transform.position) <
                            Vector3.Distance(listColliderAdversaire[i].transform.position, transform.position))
                        {
                            continuer = false;
                            listColliderAdversaire.Insert(i,c);
                        }
                        i++;
                    }
                    if (continuer)
                    {
                        listColliderAdversaire.Add(c);
                    }
                }
            }
        }


        Player testedPlayer;
        // on a la liste des victimes par ordre croissant de distance
        while (listColliderAdversaire.Count > 0)
        {
            testedPlayer = listColliderAdversaire[0].transform.GetComponentInParent<Player>();
            if (!IsThereAWallBetweenUs(testedPlayer, tCam.position))
            {
                testedPlayer.TakeDamages(DegatPompe(transform.position, Vector3.Distance(listColliderAdversaire[0].transform.position, transform.position)),
                                nomArme,
                                pShooterName,
                                _idPlayer);
                touche = true;

                RaycastHit[] hit;
                if (listColliderAdversaire.Count > 1)
                {
                    for (int i = 1; i < listColliderAdversaire.Count; i++)
                    {
                        float rapportDistance = Vector3.Distance(listColliderAdversaire[i].transform.position, transform.position) /
                                            Vector3.Distance(listColliderAdversaire[0].transform.position, transform.position);
                        Vector3 centerCapsule = listColliderAdversaire[0].bounds.center;



                        Vector3 direction = listColliderAdversaire[0].transform.position - transform.position;
                        
                        hit = Physics.SphereCastAll(centerCapsule, 0.28f * rapportDistance,
                                direction,
                                porteeMaxTir,
                                layerDamage //Layer Player
                                );
                        foreach (RaycastHit h in hit)
                        {

                            if (listColliderAdversaire.Contains(h.collider) &&
                                h.collider != listColliderAdversaire[0])
                            {
                                listColliderAdversaire.Remove(h.collider);
                            }
                        }
                    }
                }
            } 

            
            listColliderAdversaire.RemoveAt(0);
        }
        //effet du tir
        RpcEffetTir(touche);
    }


    private bool IsThereAWallBetweenUs(Player _thePlayer, Vector3 _posFireOrigin)
    {
        Vector3 direction = (_thePlayer.transform.position - 
            _thePlayer.GetComponent<CapsuleCollider>().center - tCam.position).normalized;
        Ray myRay = new Ray(tCam.position, direction);
        
        // reference au joueur qui tir
        RaycastHit hit;
        if (Physics.Raycast(myRay, out hit, 100.0f))
        {
            Debug.DrawRay(tCam.position, direction *hit.distance, Color.red, 15);
            return hit.collider.tag == "WallMap";
        }
        Debug.DrawRay(tCam.position, direction *100, Color.red, 15);
        return false;
    }




    //fonction de calcul de degat
    private float DegatPompe(Vector3 vOrigineTir, float distance)
    {
        float f = Mathf.Max(distance, 0.152f);
        f = Mathf.Min(f, porteeMaxTir);
        f =  Mathf.Log(f,10.0f) + 0.82f;
        f /= 2;
        f = (1-f) * nbDegatPrincipal + 10;
        return f;
    }

    public override void RelacheTirer(string shooterName)
    {
        if (jeTire)
        {
            jeTire = false;
            CmdAnimTir(false);
        }
        return;
    }

    public override void MajInfoWeapon()
    {
        if (OnChangeNbMun != null)
            OnChangeNbMun(nbChargeEnCours);
        nbChargeEnCoursTxt.text = nbChargeEnCours.ToString();
    }

    [ClientRpc]
    void RpcEffetTir(bool _touche)
    {
        
        GameObject effetBalle;
        if (isLocalPlayer)
        {
            effetBalle = (GameObject)Instantiate(effetTirPompe, canonFPS.position, tCam.rotation);

            if (myWeaponsManager.myPlayer.controlManette)
            {
                Utils.Import.XInputGamePadSetState(0, intensiteVibrationsG, intensiteVibrationsD);
                timerVibrations = Time.time;
                vibrations = true;
            }
            timerVibrations = Time.time;
            vibrations = true;
        }
        else
        {
            effetBalle = (GameObject)Instantiate(effetTirPompe, canonExt.position, tCam.rotation);
        }
        Destroy(effetBalle, 2.0f);
        aS.PlayOneShot(sonTirPrincipal, 0.7f);
        if (isLocalPlayer && _touche)
        {
            hitEvent();
        }
    }
}
