﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class CibleFactice : NetworkBehaviour {
    [SyncVar]
    public float life = 100;


	public void TakeDamage(float damages)
    {
        life -= damages;
        if(life < 0)
        {
            Destroy(this.gameObject);
        }
    }

}
