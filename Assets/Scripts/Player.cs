﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;
using Utils;

public class Player : NetworkBehaviour {
    private Animator CommentaireAnimator;
    private Animator PierreAnimator;
    public CanvasGroup HUDJoueur;
    public GameObject particulesSang;
    bool vibration;
    float timerTestVibra;
    public float tempTestVibra = 0.3f;
    public float intensiteVibration;

    //trainee de couleur
    public TrailRenderer myTrailRenderer;
    public Color[] tabColor = new Color[4];
    public int numCouleur;

    // Events
    public delegate void MajVie(float _life);
    public static event MajVie OnChangeLife;
    public delegate void MessageMort(bool _isActive);
    public static event MessageMort OnChangeDeath;

    public bool isFirstPerson;
    // vue model 3D + Collider
    public GameObject bodyModel3D;
    public GameObject epauleModel3D;
    public GameObject tuyauxModel3D;

    // collider
    public CapsuleCollider corpCollider;
    public BoxCollider piedsCollider;

    // camera box
    public CapsuleCollider cameraCollider;

    // trigger hitbox
    public SphereCollider headHitBox;
    public CapsuleCollider torseHitBox;
    public BoxCollider jambesHitBox;

    public bool frozen;
    public bool isDead;
    public bool controlManette;

    // Retour serie kill
    public AudioSource asCommentaires;
    private List<List<AudioClip>> listeSonCommentaires = new List<List<AudioClip>>();
    private bool isHeadShot;//1.7s
    private bool isFirstBlood; // 2s
    private bool isNbKill;//1.9s
    private bool isChangeLeader;//2.5s
    public AudioClip sonPremierSang;
    public AudioClip[] sonHeadShot = new AudioClip[2];
    public AudioClip[] joueurPrendLaTete = new AudioClip[4];
    public const int NB_KILL_CRAN_1 = 2;
    public const int NB_SONS_CRAN_1 = 3;
    public AudioClip[] sonCran1 = new AudioClip[NB_SONS_CRAN_1];
    public const int NB_KILL_CRAN_2 = 3;
    public const int NB_SONS_CRAN_2 = 2;
    public AudioClip[] sonCran2 = new AudioClip[NB_SONS_CRAN_2];
    public const int NB_KILL_CRAN_3 = 5;
    public const int NB_SONS_CRAN_3 = 1;
    public AudioClip[] sonCran3 = new AudioClip[NB_SONS_CRAN_3];
    public GameObject canvasTabScoreGO;
    private Canvas canvasTabScore;
    private Image FlagPremier;
    private Text InfoDifferencePointsDuPremier;
    private Text InfoSerieKill;
    private float timerCommentaire;
    public float tempMaxEntreCommentaire;
    private Image imageCommentaireHUD;
    public const int NB_IMAGES = 9;
    public Sprite[] tabImagesCommentaires = new Sprite[NB_IMAGES];
    private List<int> indexImagesToUse = new List<int>();

    [SyncVar]
    public string pseudo = "Johndoe";
    public const float maxLife = 100.0f;

    [SyncVar(hook = "OnChangeLifeValue")]
    float life = maxLife; 
    Text vieTxt;
    KillReportManager KRM;
    public WeaponsManager WM;
    BoostPack myBoostPack;
    public UIManager myUIManager;
    // audio source mouvement
    public AudioSource asMouvement;
    public AudioClip sonMortJoueur;
    public AudioClip sonRespawnJoueur;
    public AudioClip sonFinPartie;

    // audio source impact joueur
    public AudioSource asImpacts;
    public AudioClip sonImpact;

    float timerReswpawn;
    public float tempsAvantRespawn;

    public NetworkInstanceId myIdPlayer;

    public override void OnStartLocalPlayer()
    {
        HUDJoueur= GameObject.Find("Canvas/HUD").GetComponent<CanvasGroup>();
        HUDJoueur.alpha = 1.0f;
        if (GameObject.Find("Musique Ambiance") != null)
        {
            GameObject.Find("Musique Ambiance").SetActive(false);
        }
        vieTxt = GameObject.Find("Canvas/HUD/VieTxt").GetComponent<Text>();
        KRM = GameObject.Find("KillReportManager").GetComponent<KillReportManager>();// trouver pourquoi ça ne marche pas
        myBoostPack = GetComponent<BoostPack>();
        isDead = false;
        frozen = true;
        //partie join pour l'ecran personnage
        AudioListener aL = GameObject.Find("NetworkManager").GetComponent<AudioListener>();
        aL.enabled = false;

        if(GameObject.Find("InfoJoueur") != null)
        {
            CmdSetPseudo(GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().nomGladiateur);
            controlManette = GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().controlManette;

        }
        else
        {
            controlManette = true;
        }
        // Destroy(GameObject.Find("InfoJoueur"), 1.0f);

        //Disparition du body du joueur 
        bodyModel3D.SetActive(false);
        epauleModel3D.SetActive(false);
        tuyauxModel3D.SetActive(false);
        vibration = false;

        GameObject.Find("Canvas/InfoScore").GetComponent<Canvas>().enabled = true; 
        // systeme infoClassement
        FlagPremier = GameObject.Find("Canvas/InfoScore/InfoClassement/Fond/Dessus/FlagLeader").GetComponent<Image>();
        InfoDifferencePointsDuPremier = GameObject.Find("Canvas/InfoScore/InfoClassement/Fond/Dessus/retardPremier").
                                            GetComponent<Text>();
        InfoSerieKill = GameObject.Find("Canvas/InfoScore/InfoClassement/Fond/Dessus/SerieKill").
                                            GetComponent<Text>();

        CommentaireAnimator = GameObject.Find("Canvas/TextPierre").GetComponent<Animator>();
        imageCommentaireHUD = GameObject.Find("Canvas/TextPierre").GetComponent<Image>();
    }

    void Start()
    {
        if (isLocalPlayer)
        {
            vieTxt.text = life.ToString();
            canvasTabScore = GameObject.Find("Canvas/TableauScore").GetComponent<Canvas>();
            isFirstPerson = true;
            
        }
        else
        {
            isFirstPerson = false;
        }
        myIdPlayer = GetComponent<NetworkIdentity>().netId;
        KRM = GameObject.Find("KillReportManager").GetComponent<KillReportManager>();
        //KRM.RegisterPlayer(myIdPlayer);
        Invoke("TempoName", 1.0f);
        for (int i = 0; i < 4; i++)
            listeSonCommentaires.Add(new List<AudioClip>());

       
    }

    void TempoName()
    {
        if (pseudo != "Spectateur")// pour le serveur
        {
            KRM.RegisterPlayer(myIdPlayer);
        }
        else
        {
            CmdSpectateur();
        }  
    }

    [Command]
    public void CmdSpectateur()
    {
        RpcSpectateur();
    }
    [ClientRpc]
    public void RpcSpectateur()
    {
        if (isLocalPlayer)
        {
            frozen = false;
            transform.position = new Vector3(-12.0f, 24.0f, 35.0f);

            bodyModel3D.SetActive(false);
            epauleModel3D.SetActive(false);
            tuyauxModel3D.SetActive(false);

            headHitBox.enabled = false;
            torseHitBox.enabled = false;
            jambesHitBox.enabled = false;

            corpCollider.enabled = false;
            cameraCollider.enabled = false;
            piedsCollider.enabled = false;
            GetComponent<GravityPack>().intensiteGravite = 0.0f;
            WM.DesactivateWeaponModel();
            WM.isActive = false;
            GameObject.Find("Canvas/HUD").GetComponent<CanvasGroup>().alpha = 0.0f;
        }

    
    }

    void Update()
    {
        if (!isLocalPlayer) return;
        if (frozen) return;
        vieTxt.text = life.ToString("F0");

        if (isDead && Time.time - timerReswpawn > tempsAvantRespawn)
        {
            isDead = false;
            CmdRespawn();
            myBoostPack.InitBoosts();
        }

        if (Input.GetButtonDown("ResetPos") || Input.GetKey(KeyCode.M))
        {
            CmdRespawn();
        }

        //reset vibration
        if (vibration && Time.time -timerTestVibra > tempTestVibra)
        {
            Import.XInputGamePadSetState(0,0.0f, 0.0f);
            vibration = false;
        }

        if (Input.GetButtonDown("TabScore"))
        {
            canvasTabScore.enabled=true;
            KRM.MajTableauScore();
        }
        if (Input.GetButtonUp("TabScore"))
        {
            canvasTabScore.enabled = false;
        }

        // pour essai krm
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            KRM.AddStatKill(myIdPlayer);
        }

        if (!asCommentaires.isPlaying || Time.time - timerCommentaire > tempMaxEntreCommentaire)
        {
            timerCommentaire = Time.time;
            if (listeSonCommentaires[0].Count > 0)
            {
                asCommentaires.clip = listeSonCommentaires[0][0];
                asCommentaires.Play();
                imageCommentaireHUD.sprite = tabImagesCommentaires[indexImagesToUse[0]];
                Color c = imageCommentaireHUD.color;
                c.a = 1.0f;
                imageCommentaireHUD.color = c;
                CommentaireAnimator.SetTrigger("lancer");
                indexImagesToUse.RemoveAt(0);
                listeSonCommentaires[0].RemoveAt(0);
            }
            else if (listeSonCommentaires[1].Count > 0)
            {
                asCommentaires.clip = listeSonCommentaires[1][0];
                asCommentaires.Play();
                imageCommentaireHUD.sprite = tabImagesCommentaires[indexImagesToUse[0]];
                Color c = imageCommentaireHUD.color;
                c.a = 1.0f;
                imageCommentaireHUD.color = c;
                CommentaireAnimator.SetTrigger("lancer");
                indexImagesToUse.RemoveAt(0);
                listeSonCommentaires[1].RemoveAt(0);
            }
            else if (listeSonCommentaires[2].Count > 0)
            {
                asCommentaires.clip = listeSonCommentaires[2][0];
                asCommentaires.Play();
                imageCommentaireHUD.sprite = tabImagesCommentaires[indexImagesToUse[0]];
                Color c = imageCommentaireHUD.color;
                c.a = 1.0f;
                imageCommentaireHUD.color = c;
                CommentaireAnimator.SetTrigger("lancer");
                indexImagesToUse.RemoveAt(0);
                listeSonCommentaires[2].RemoveAt(0);
            }
            else if (listeSonCommentaires[3].Count > 0)
            {
                asCommentaires.clip = listeSonCommentaires[3][0];
                asCommentaires.Play();
                listeSonCommentaires[3].RemoveAt(0);
            }

        }
    }

    

    public void TakeDamages(float damages, string nomArme, string nomTueur, NetworkInstanceId _idPlayer)
    {
        if (life > damages)
        {
            life -= damages;
            RpcAddThreat(_idPlayer);
            RpcBlood();
            PlayImpactSound();
        }
        else
        {
            life = 0;
            KRM = GameObject.Find("KillReportManager").GetComponent<KillReportManager>();
            KRM.AddKRM(pseudo, nomTueur, nomArme);
            RpcAddScore(myIdPlayer, _idPlayer);
            if(myIdPlayer != _idPlayer)
                KRM.AddStatKill(_idPlayer);

            KRM.ResetSerieKill(myIdPlayer);
            GameObject packMuni = (GameObject)Instantiate(NetworkManager.singleton.spawnPrefabs[8], transform.position -(transform.forward * 0.23f), transform.rotation);
            packMuni.GetComponent<AmmoPack>().SetRotationVector(Vector3.up);
            NetworkServer.Spawn(packMuni);
            RpcDeath();
        }
    }

    [ClientRpc]
    void RpcAddScore(NetworkInstanceId v, NetworkInstanceId t)
    {
        KRM.AddScore(v, t);
    }

    [Command]
    public void CmdAddExplosionForce(Vector3 vecForceExlo)
    {
        RpcAddExplosionForce(vecForceExlo);
    }
    [ClientRpc]
    public void RpcAddExplosionForce(Vector3 vecForceExlo)
    {
        if (!isLocalPlayer) return;
        GetComponent<Rigidbody>().AddForce(vecForceExlo, ForceMode.VelocityChange);
    }

    [Command]
    void CmdPack()
    {
        GameObject packMuni = (GameObject)Instantiate(NetworkManager.singleton.spawnPrefabs[8], transform.position, transform.rotation);
        packMuni.GetComponent<AmmoPack>().SetRotationVector(Vector3.up);
        NetworkServer.Spawn(packMuni);
    }

    [Command]
    void CmdRespawn()
    {
        life = maxLife;
        RpcRespawn();
    }

    [ClientRpc]
    public void RpcRespawn()
    {
        Transform t;
        int layerJoueur = 1 << 8;
        if (isLocalPlayer)
        {
            if (OnChangeDeath != null)
                OnChangeDeath(false);
            WM.ResetWeaponsPack();
            GetComponent<ShipMovement>().m_Rigidbody.isKinematic = false;
        }
        
        int r = Random.Range(0, 5);
        t = GameObject.Find("NetworkManager").transform.GetChild(r);
        RaycastHit hit;
        // test pour eviter de spawn pres des joueurs actifs
        int i = 0;
        while (Physics.OverlapSphere(t.position, 15.0f, layerJoueur).Length > 0 && i<5)
        {
            r = Random.Range(0, 5);
            t = GameObject.Find("NetworkManager").transform.GetChild(r);
            i++;
        }

        transform.position = t.position;
            WM.ReactivateWeaponModel();


        Invoke("TempoVisible", 0.1f);
        asMouvement.pitch = 1.0f;
        asMouvement.PlayOneShot(sonRespawnJoueur, 0.7f);
    }

    void TempoVisible()
    {
        if (!isLocalPlayer)
        {
            bodyModel3D.SetActive(true);
            epauleModel3D.SetActive(true);
            tuyauxModel3D.SetActive(true);

        }
            headHitBox.enabled = true;
            torseHitBox.enabled = true;
            jambesHitBox.enabled = true;

        corpCollider.enabled = true;
        cameraCollider.enabled = true;
        piedsCollider.enabled = true;

       
    }

   [Command]
   public void CmdUpdateListPlayerScore(NetworkInstanceId myId)
    {
        RpcUpdateListPlayerScore(myId);
    }


    [ClientRpc]
    public void RpcUpdateListPlayerScore(NetworkInstanceId newId)
    {
        //KRM.RegisterPlayer(newId);
    }

    void OnChangeLifeValue(float life)
    {
        this.life = life;
        if (isLocalPlayer)
        {
            if (OnChangeLife != null)
                OnChangeLife(life / 100.0f);
        }
    }

    [ClientRpc]
    public void RpcDeath()
    {
        asMouvement.pitch = 1.0f;
        asMouvement.PlayOneShot(sonMortJoueur, 1.0f);
        if (!isLocalPlayer)
        {
            bodyModel3D.SetActive(false);
            epauleModel3D.SetActive(false);
            tuyauxModel3D.SetActive(false);


        }

        headHitBox.enabled = false;
        torseHitBox.enabled = false;
        jambesHitBox.enabled = false;

        corpCollider.enabled = false;
        cameraCollider.enabled = false;
        piedsCollider.enabled = false;

      

        WM.DesactivateWeaponModel();
        if (isLocalPlayer)
        {
            timerReswpawn = Time.time;
            isDead = true;
            GetComponent<ShipMovement>().m_Rigidbody.isKinematic = true;
            if (controlManette)
            {
                timerTestVibra = Time.time;
                vibration = true;
                Import.XInputGamePadSetState(0, 0.5f, 0.5f);
            }

            if (OnChangeDeath != null)
                OnChangeDeath(true);
            myUIManager.ClearThreatTTL();
        }

    }

    [Command]
    void CmdSetPseudo(string s)
    {
        pseudo = s;
    }


    [ClientRpc]
    void RpcAddThreat(NetworkInstanceId _idPlayer)
    {
        if (isLocalPlayer)
        {
            myUIManager.AddThreat(_idPlayer);
        }
    }

    void PlayImpactSound()
    {
        if (isLocalPlayer)
        {
            asImpacts.PlayOneShot(sonImpact,0.7f);
            if (controlManette)
            {
                timerTestVibra = Time.time;
                vibration = true;
                Import.XInputGamePadSetState(0, intensiteVibration, intensiteVibration);
            }
        }
    }

    [ClientRpc]
    public void RpcPlayPublicSound(int nbKill)
    {
        if (!isLocalPlayer) return;
        switch (nbKill)
        {
            case NB_KILL_CRAN_1:
                int random1 = Random.Range(0, NB_SONS_CRAN_1);
                listeSonCommentaires[2].Add(sonCran1[random1]);
                indexImagesToUse.Add(3+random1);
                break;
            case NB_KILL_CRAN_2:
                int random2 = Random.Range(0, NB_SONS_CRAN_2);
                listeSonCommentaires[2].Add(sonCran2[random2]);
                indexImagesToUse.Add(6 + random2);
                break;
            case NB_KILL_CRAN_3:
                int random3 = Random.Range(0, NB_SONS_CRAN_3);
                listeSonCommentaires[2].Add(sonCran3[random3]);
                indexImagesToUse.Add(8 + random3);
                break;
            default:
                break;
        }
        Debug.Log("ajoutSon NBKILL");
    }

    // met a jour la couleur de sa trainee
    [Command]
    public void CmdCouleurTrainee(int _numColor)
    {
        RpcCouleurTrainee(_numColor);
    }

    [ClientRpc]
    public void RpcCouleurTrainee(int _numCouleur)
    {
        numCouleur = _numCouleur;
        myTrailRenderer.material.SetColor("_TintColor", tabColor[numCouleur]);

        if (isLocalPlayer)
        {
            float f = GameObject.Find("Fond").GetComponent<Image>().color.a;
            Color c = tabColor[numCouleur];
            c.a = f;
            GameObject.Find("Fond").GetComponent<Image>().color = c;
        }
    }

    [ClientRpc]
    public void RpcBackAllPlayerColor(int _numColor)
    {

        myTrailRenderer.material.SetColor("_TintColor", tabColor[_numColor]);

    }

    [ClientRpc]
    public void RpcSetFrozen(bool b)
    {
        frozen = b;
    }

    [ClientRpc]
    public void RpcSonGrille(bool b)
    {
        asMouvement.pitch = 1.0f;
        if (b)
        {
            asImpacts.PlayOneShot(sonRespawnJoueur, 0.3f);
        }
        else
        {
            if (isLocalPlayer)
            {
                GameObject.Find("map JO").GetComponent<AudioSource>().enabled = false;
                asImpacts.PlayOneShot(sonFinPartie, 0.7f);
            }
        }
    }

    [ClientRpc]
    public void RpcBlood()
    {
        GameObject effetSang = (GameObject)Instantiate(particulesSang, transform.position, Quaternion.identity);
        Destroy(effetSang, 0.5f);
    }

    // mise a jour du premier et du nb de kill a faire
    //Dans KRM EnvoiInfoClassement
    [ClientRpc]
    public void RpcInfoClassement(int _indexPremier, int _nbPointDuPremier)
    {
        if (isLocalPlayer)
        {
            Color c = tabColor[_indexPremier];
            c.a = FlagPremier.color.a;
            FlagPremier.color = c;
            if(_nbPointDuPremier <= 0)
            {
                InfoDifferencePointsDuPremier.text = "t'es premier !";
            }
            else
            {
                InfoDifferencePointsDuPremier.text = _nbPointDuPremier.ToString() +
                    " point" + (_nbPointDuPremier > 1 ? "s" : "") + " d'écart";
            }
        }
    }

    // jouer le firstBlood

    [ClientRpc]
    public void RpcPlayFirstBlood()
    {
        if (!isLocalPlayer) return;
        listeSonCommentaires[1].Add(sonPremierSang);
        indexImagesToUse.Add(2);
        Debug.Log("ajoutSon Premier cadavre");
    }



    // changement de leader
    [ClientRpc]
    public void RpcPlayNewLeader(int _indexLeader)
    {
        if (!isLocalPlayer) return;
        listeSonCommentaires[3].Add(joueurPrendLaTete[_indexLeader]);
        Debug.Log("ajoutSon New leader");
    }


    [ClientRpc]
    public void RpcPlayHeadShotSound()
    {
        int random = Random.Range(0, 2);
        listeSonCommentaires[0].Add(sonHeadShot[random]);
        indexImagesToUse.Add(random);
        Debug.Log("ajoutSon HEadshot");
    }

    // mise a jour de la serie de kill
    [ClientRpc]
    public void RpcUpSerieKill(int _nbKill)
    {
        if (isLocalPlayer)
        {
            InfoSerieKill.text = "Serie de tués : " + _nbKill;
        }
    }

   

}
