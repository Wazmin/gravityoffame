﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Roquette : NetworkBehaviour {


    public float vitesse = 1.0f;
    public float distanceMaxExplosion;
    public float degatMaxExplosion;
    public float facteurForceExplo;
    public AudioSource aS;
    public AudioClip ac;
    public GameObject effetExplo;
    public string nomLanceur;
    public NetworkInstanceId idPlayer;
    private KillReportManager KRM;

    void Start()
    {
        KRM = GameObject.Find("KillReportManager").GetComponent<KillReportManager>();
    }
   

    void OnCollisionEnter(Collision collision)
    {
        if(aS.isActiveAndEnabled)
        aS.PlayOneShot(ac);

        GameObject explosion = (GameObject)Instantiate(effetExplo, transform.position, Quaternion.identity);
        aS.pitch = 1.0f;

        if (isServer)
        {
            // creation physic de la sphere d'explosion
            int layerExplo = 1 << 9;
            Collider[] tColliders = Physics.OverlapSphere(transform.position,
                                                            distanceMaxExplosion,
                                                            layerExplo);
            Vector3 vPosCollider;
            float distance;
            List<Collider> detectedColliders = new List<Collider>();
            List<NetworkInstanceId> lNetId = new List<NetworkInstanceId>();
            NetworkInstanceId tmpNetId;

            foreach (Collider collider in tColliders)
            {
                if (collider.tag == "Player" 
                    && !detectedColliders.Contains(collider))
                {
                    tmpNetId = collider.transform.GetComponentInParent<NetworkIdentity>().netId;
                    // on fait en sorte que le joueur ne prenne qu'une seule fois
                    // des degats
                    if (!lNetId.Contains(tmpNetId))
                    {
                        detectedColliders.Add(collider);
                        lNetId.Add(tmpNetId);
                        distance = Vector3.Distance(transform.position, collider.transform.position);
                        collider.transform.GetComponentInParent<Player>().TakeDamages(
                            DegatsExplosions(distance),
                            "Bazooka",
                            nomLanceur,
                            idPlayer);
                        GameObject originPlayer = NetworkServer.FindLocalObject(idPlayer);
                        originPlayer.GetComponent<UIManager>().Hit();
                        vPosCollider = collider.transform.position;
                        vPosCollider -= transform.position;
                        vPosCollider.Normalize();

                        //calcul de la puissance reçue
                        distance = (1 - (distance / distanceMaxExplosion)) * facteurForceExplo;
                        collider.transform.GetComponent<Player>().RpcAddExplosionForce(
                               vPosCollider * distance);
                    }
                }

            }
        }
       


        Destroy(explosion, 2.0f);
        this.gameObject.SetActive(false);
        Destroy(this.gameObject,0.5f);
      
    }


    private float DegatsExplosions(float _distance)
    {
        return Mathf.Max((1 -(_distance / distanceMaxExplosion)) * degatMaxExplosion,0.0f);
    }

    //[Command]
    //void CmdTakeDamage()
    //{

    //}

}
