﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SolColliderScript : NetworkBehaviour {
    public PlayerAnimations myPlayerAnimation;
    public GravityPack gp;
    AudioSource aS;
    public AudioClip sonSautReception;
    public int numFonctionnement;

    void Start()
    {
        if (numFonctionnement != 1) return;
        aS = transform.parent.GetComponent<AudioSource>();
    }

    public override void OnStartLocalPlayer()
    {
       


    }
    void OnCollisionEnter(Collision collision)
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag =="WallMap")
        {
            if(numFonctionnement == 1)
            {
                aS.PlayOneShot(sonSautReception, 0.6f);
                gp.grounded = true;
                myPlayerAnimation.SetIsGrounded(true);
                gp.canDoubleJump = true;
            }
            else if(numFonctionnement == 2)
            {
                //myPlayerAnimation.SetIsGrounded(true);
            }
           
        }  
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "WallMap")
        {
            gp.grounded = false;
        }

    }

    // Use this for initialization

    // Update is called once per frame
    void Update () {
	
	}
}
