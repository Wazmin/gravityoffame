﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerAnimations : NetworkBehaviour {
    public Animator myAnimator;
    public bool jump;
    public bool fall;
    public bool grounded;
    public int nbParam = 4;

        
    public NetworkAnimator netAnim;

    public override void OnStartLocalPlayer()
    {
        for (int i = 0; i < nbParam; i++)
        {
            netAnim.SetParameterAutoSend(i, true);
        }
    }

    public override void PreStartClient()
    {
        for (int i = 0; i < nbParam; i++)
        {
            netAnim.SetParameterAutoSend(i, true);
        }
    }

    public void SetVerticalSpeed(float f)
    {
        myAnimator.SetFloat("BlendStopToRun", f);
    }

    public void SetIsGrounded(bool b)
    {
        //netAnim.SetTrigger("trigReception");
        myAnimator.SetBool("isGrounded", b);
        if (b)myAnimator.SetBool("isFalling", false);
    }

    public void SetHorizontalSpeed(float f)
    {
        myAnimator.SetFloat("PasChasses",f);
    }

    public void SetJump()
    {
        if (!isLocalPlayer) return;
        netAnim.SetTrigger("trigJump");

    }

    public void ChangerArme(int numWeapon)
    {
        myAnimator.SetInteger("numWeapon", numWeapon);
    }

    // via trigger
    public void Tirer()
    {
        netAnim.SetTrigger("trigTir");
    }

    public void Recharger()
    {
        netAnim.SetTrigger("trigReload");
    }

    public void SetReload(bool b)
    {
        myAnimator.SetBool("isReloading", b);
    }

    public void SetInclinaison(float inclinaison)
    {
        myAnimator.SetFloat("Inclinaison", inclinaison);
    }

    public void IsFiring(bool b)
    {
        myAnimator.SetBool("isFiring",b);
    }


    // quand chute
    public void SetIsFalling(bool b)
    {
        myAnimator.SetBool("isFalling", b);
        fall = b;
    }

    public void SetChangeGravity(int _numChangeGrav)
    {
        myAnimator.SetInteger("numChangeGrav", _numChangeGrav);
    }
}
