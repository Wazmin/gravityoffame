﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;
using System.IO;


public  class StatsTest : MonoBehaviour {
    public bool isActive;
    private string fileName = "statsJoueur.txt";

    public int nbSaut = 0;
    public int nbDoubleSaut = 0;
    public int nbTir = 0;
    public int nbTirTouche = 0;
    public int nbChangementGravite = 0;
    public int nbChangementGraviteDevant = 0;
    public int nbChangementGraviteArriere = 0;
    public int nbChangementGraviteLeft = 0;
    public int nbChangementGraviteRight = 0;
 


    void OnDestroy()
    {
        if (isActive)
        {
            int i;
            string fileName;

            do
            {
                i = UnityEngine.Random.Range(0, 1000000);
                fileName = "statJoueur_" + i.ToString() + ".txt";
            } while (File.Exists(fileName));

            System.IO.StreamWriter file = new System.IO.StreamWriter(fileName);
            file.WriteLine("-------------- DEBUT DONNEES --------------");
            file.WriteLine("Saut : " + nbSaut);
            file.WriteLine("Double saut : " + nbDoubleSaut);
            file.WriteLine("Tir : " + nbTir);
            file.WriteLine("Tir touché : " + nbTirTouche);
            file.WriteLine("Changement Gravité : " + nbChangementGravite);
            file.WriteLine("Changement Gravité Devant : " + nbChangementGraviteDevant);
            file.WriteLine("Changement Gravité Arriere : " + nbChangementGraviteArriere);
            file.WriteLine("Changement Gravité Gauche : " + nbChangementGraviteLeft);
            file.WriteLine("Changement Gravité Droit : " + nbChangementGraviteRight);
            file.WriteLine("--------------- FIN DONNEES ---------------");
            file.WriteLine(" ");
            file.Close();
        }
        
    }

}
