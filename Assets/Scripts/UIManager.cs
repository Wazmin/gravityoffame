﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


struct CursorDmg
{
    public float _timeToLive;
    public GameObject _anchorCursor;
}

public class UIManager : NetworkBehaviour {
    public int nbJoueursMax;
    public AudioSource asEvent;
    public AudioClip sonEnnemiTouche;

    private const float df = 255.0f;
    public const float ALPHA_ACTIF = 1.0f;
    public const float ALPHA_INACTIF = 0.75f;
    private Vector4 COLOR_BAZOOKA = new Vector4(5/df, 160/df,212/df,180/df);
    private Vector4 COLOR_OS = new Vector4(9/df, 72/df,96/df,180/df);
    private Vector4 COLOR_POMPE = new Vector4(30/df,96/df,108/df,210/df);
    private Vector4 COLOR_MITRA = new Vector4(255/df,255/df,255/df,210/df);
    private Vector4 COLOR_SELECTED = new Vector4(255/df,164/df,0/df,180/df);
    private Vector4 COLOR_FULL_LIFE = new Vector4(66 / df, 255 / df, 98 / df, 180 / df);
    private Vector4 COLOR_LOW_LIFE = new Vector4(249/df,50/df,50/df,180/df);
    public float timeMaxCursorDmg;
    private Canvas myCanvas;
    public GameObject CursorDamage;
    public GameObject CameraGO;
    private Camera myCamera;
    private Vector3  centerOfCanvas;
    private bool mustRefresh;

    // effet clignoement proche mort
    private bool blink;
    private float timerBlink;
    public float tempsBlink;
    private float nbLife;
    private string tmpString;

    static UIManager _manager;
    public static UIManager Get
    {
        get
        {
            if (_manager == null)
                _manager = GameObject.FindObjectOfType<UIManager>();
            return _manager;
        }
    }

    //Liste de curseur damage actif
    List<NetworkInstanceId> lNetIdEnnemi = new List<NetworkInstanceId>();
    List<CursorDmg> lCursorDmg = new List<CursorDmg>();

    // HUD
    private CanvasGroup _hud;

    // BLOOD 
    private Image _imageBlood;
    private float timerBlood;
    public float tempsBlood;
    private bool isBlooding;
    Color colorBlood;

    // Boost
    private Image _imageBarreBoost;
    private Image _imageFlouBoost;

    // Shield
    private Image _imageBarreVie;
    private Image _imageBarreVieVide;
    private Image _iconeCoeur;

    //Bazooka
    private Image _imageBarreBazooka;
    private Image _imageBarreVideBazooka;
    //OneShooter
    private Image _imageBarreOneShooter;
    private Image _imageBarreVideOneShooter;
    //Mitrailleuse
    private Image _imageBarreMitrailleuse;
    private Image _imageBarreVideMitrailleuse;
    //Pompe
    private Image _imageBarrePompe;
    private Image _imageBarreVidePompe;
    //Curseur touché
    private Image _imageCurseurTouche;

    //texte de mort
    private Text _textMort;

    public float tmpCurseurTouche;
    private float timerCurseurTouche;

    private bool isHitting;

    // systeme GameManager
    private Canvas _canvasTabScore;
    private GameObject _BtnRetourMenu;
    public Text _txtMessageGameManager;
    public Text _chronoPartie;



    // Use this for initialization
    void Start () {

        //enregistrement des composants externes
        BoostPack.OnChangeNbBoost += MajBoost;
        BoostPack.OnChangeTimeBoost += ActiveBoost;
        Player.OnChangeLife += MajLife;
        Player.OnChangeLife += AfficheBlood;
        Player.OnChangeDeath += AfficheMsgMort;
        Bazooka.OnChangeNbMun += MajBazooka;
        Mitrailleuse.OnChangeNbMun += MajMitrailleuse;
        OneShooter.OnChangeNbMun += MajOneShooter;
        FusilPompe.OnChangeNbMun += MajPompe;
        Weapon.OnHitSometing += Hit;
        WeaponsManager.OnChangeSelectedWeapon += PutAlphaActive;

        //GameObject canvasGO = GameObject.Find("Canvas").GetComponent<GameObject>();
        myCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        centerOfCanvas = GameObject.Find("Canvas/CenterOfCanvas").transform.position;
        _hud = GameObject.Find("Canvas/HUD").GetComponent<CanvasGroup>();
        _imageBarreBoost = GameObject.Find("Canvas/HUD/Boost/barrePleine").GetComponent<Image>();
        _imageFlouBoost = GameObject.Find("Canvas/HUD/FlouBoost").GetComponent<Image>();

        _imageBarreBazooka = GameObject.Find("Canvas/HUD/ChargeBazooka/barrePleine").GetComponent<Image>();
        _imageBarreVideBazooka = GameObject.Find("Canvas/HUD/ChargeBazooka/barreVide").GetComponent<Image>();

        _imageBarreMitrailleuse = GameObject.Find("Canvas/HUD/ChargesMitrailleuse/barrePleine").GetComponent<Image>();
        
        _imageBarreOneShooter = GameObject.Find("Canvas/HUD/ChargesOs/barrePleine").GetComponent<Image>();
        _imageBarreVideOneShooter = GameObject.Find("Canvas/HUD/ChargesOs/barreVide").GetComponent<Image>();

        _imageBarrePompe = GameObject.Find("Canvas/HUD/ChargesPompe/barrePleine").GetComponent<Image>();
        _imageBarreVidePompe = GameObject.Find("Canvas/HUD/ChargesPompe/barreVide").GetComponent<Image>();

        _imageCurseurTouche = GameObject.Find("Canvas/HUD/CurseurTouche").GetComponent<Image>();

        _imageBarreVie=GameObject.Find("Canvas/HUD/Vie/barrePleine").GetComponent<Image>();
        _imageBarreVieVide = GameObject.Find("Canvas/HUD/Vie/barreVide").GetComponent<Image>();
        _iconeCoeur = GameObject.Find("Canvas/HUD/IcoCoeur").GetComponent<Image>();

        _textMort = GameObject.Find("Canvas/TextMort").GetComponent<Text>();
        _textMort.enabled = false;

        _imageBlood = GameObject.Find("Canvas/HUD/Blood").GetComponent<Image>();

        _canvasTabScore = GameObject.Find("Canvas/TableauScore").GetComponent<Canvas>();
        _BtnRetourMenu = GameObject.Find("Canvas/TableauScore/Panel/btnRetourMenu");
        _BtnRetourMenu.SetActive(false);
        _txtMessageGameManager = GameObject.Find("Canvas/TextGameManager").GetComponent<Text>();
        _chronoPartie = GameObject.Find("Canvas/TxtChronoPartie").GetComponent<Text>();


        if (!isLocalPlayer) return;
        nbLife = 1.0f;
        timerCurseurTouche = Time.time;
        timerBlink = timerCurseurTouche;
        myCamera = CameraGO.GetComponent<Camera>();
        mustRefresh = false;
        colorBlood = _imageBlood.color;
        GameObject.Find("Canvas/InfoScore").GetComponent<Canvas>().enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (!isLocalPlayer) return;

        if (isHitting && Time.time - timerCurseurTouche > tmpCurseurTouche)
        {
            ResetHit();
        }

        if(mustRefresh)
        {
            if (lNetIdEnnemi.Count > 0)
                RefreshThreatTTL();
            mustRefresh = false;
        }
        else
        {
            mustRefresh = true;
        }

        //cligno vie
        if(nbLife <= 0.3f)
        {
            if(Time.time -timerBlink > tempsBlink)
            {
                timerBlink = Time.time;
               BlinkLife(blink);
                blink = !blink;     
            }
        }

        if (isBlooding)
        {
            if (Time.time - timerBlood > tempsBlood)
            {
                isBlooding = false;
                _imageBlood.enabled = false;
            }
            else
            {
                colorBlood.a =  1.0f -(Time.time - timerBlood) / tempsBlood;
                _imageBlood.color = colorBlood;
            }
        }
    }

    // UI Dose Boost
    private void MajBoost(int _nbBoost)
    {
        //nbBoost += _nbBoost;
        float fillToDo = 0.0f;
        switch (_nbBoost)
        {
            case 0:
                fillToDo = 0.0f;
                break;
            case 1:
                fillToDo = 0.33f;
                break;
            case 2:
                fillToDo = 0.67f;
                break;
            case 3:
                fillToDo = 1.0f;
                break;
            default:
                fillToDo = 1.5f; //bug visuel
                break;
        }
        _imageBarreBoost.fillAmount = fillToDo;
    }

    // UI FlouBoost
    private void ActiveBoost(float tauxTempsRestant)
    {
        Color c = _imageFlouBoost.color;
        c.a =  Mathf.Min(1.0f ,Mathf.Max(1.0f-tauxTempsRestant,0.0f));
        _imageFlouBoost.color = c;

    }

    // UI Shield
    private void MajLife(float valLife)
    {
        nbLife = valLife;
        if (valLife <= 0.3f)
        {
            _imageBarreVie.color = COLOR_LOW_LIFE;
            _imageBarreVieVide.color = COLOR_LOW_LIFE;
            _iconeCoeur.color = COLOR_LOW_LIFE;
        }
        else
        {
            _imageBarreVie.color = COLOR_FULL_LIFE;
            _imageBarreVieVide.color = COLOR_FULL_LIFE;
            _iconeCoeur.color = COLOR_FULL_LIFE;
        }
        _imageBarreVie.fillAmount = valLife;
    }

    private void BlinkLife(bool isVisible)
    {
        Vector4 color = COLOR_LOW_LIFE;
        color.w = isVisible ? 1.0f : 0.0f;
        _iconeCoeur.color = color;
    }

    // UI Bazooka
    private void MajBazooka(bool isCharged)
    {
        _imageBarreBazooka.fillAmount = isCharged ? 1.0f : 0.0f;  
    }

    // UI OneShooter charge = nbCharge + qteEnergie
    private void MajOneShooter(float charge)
    {
        _imageBarreOneShooter.fillAmount = charge;
    }

    // UI Fusil a Pompe
    private void MajPompe(int _nbCartouches)
    {
        float fillToDo = 0.0f;
        switch (_nbCartouches)
        {
            case 0:
                fillToDo = 0.0f;
                break;
            case 1:
                fillToDo = 0.1f;
                break;
            case 2:
                fillToDo = 0.2f;
                break;
            case 3:
                fillToDo = 0.3f;
                break;
            case 4:
                fillToDo = 0.4f;
                break;
            case 5:
                fillToDo = 0.49f;
                break;
            case 6:
                fillToDo = 0.6f;
                break;
            case 7:
                fillToDo = 0.71f;
                break;
            case 8:
                fillToDo = 0.8f;
                break;
            case 9:
                fillToDo = 0.91f;
                break;
            case 10:
                fillToDo = 1.0f;
                break;
            default:
                fillToDo = 1.5f; //bug visuel
                break;
        }
        _imageBarrePompe.fillAmount = fillToDo;
    }

    // Ui Mitrailleuse fonction tres moche
    private void MajMitrailleuse(int _nbMunition)
    {
        float fillToDo = 0.0f;
        switch (_nbMunition)
        {
            case 0:
                fillToDo = 0.0f;
                break;
            case 1:
                fillToDo = 0.02f;
                break;
            case 2:
                fillToDo = 0.05f;
                break;
            case 3:
                fillToDo = 0.07f;
                break;
            case 4:
                fillToDo = 0.1f;
                break;
            case 5:
                fillToDo = 0.13f;
                break;
            case 6:
                fillToDo = 0.16f;
                break;
            case 7:
                fillToDo = 0.19f;
                break;
            case 8:
                fillToDo = 0.23f;
                break;
            case 9:
                fillToDo = 0.26f;
                break;
            case 10:
                fillToDo = 0.33f;
                break;
            case 11:
                fillToDo = 0.38f;
                break;
            case 12:
                fillToDo = 0.41f;
                break;
            case 13:
                fillToDo = 0.44f;
                break;
            case 14:
                fillToDo = 0.47f;
                break;
            case 15:
                fillToDo = 0.5f;
                break;
            case 16:
                fillToDo = 0.53f;
                break;
            case 17:
                fillToDo = 0.56f;
                break;
            case 18:
                fillToDo = 0.59f;
                break;
            case 19:
                fillToDo = 0.62f;
                break;
            case 20:
                fillToDo = 0.65f;
                break;
            case 21:
                fillToDo = 0.74f;
                break;
            case 22:
                fillToDo = 0.77f;
                break;
            case 23:
                fillToDo = 0.80f;
                break;
            case 24:
                fillToDo = 0.83f;
                break;
            case 25:
                fillToDo = 0.86f;
                break;
            case 26:
                fillToDo = 0.89f;
                break;
            case 27:
                fillToDo = 0.92f;
                break;
            case 28:
                fillToDo = 0.95f;
                break;
            case 29:
                fillToDo = 0.97f;
                break;
            case 30:
                fillToDo = 1.0f;
                break;
            default:
                fillToDo = 1.5f; //bug visuel
                break;
        }
        _imageBarreMitrailleuse.fillAmount = fillToDo;
    }

    public void Hit()
    {
        if (isLocalPlayer)
        {
            timerCurseurTouche = Time.time;
            isHitting = true;
            _imageCurseurTouche.enabled = true;
            asEvent.PlayOneShot(sonEnnemiTouche);
        }
    }

    private void ResetHit()
    {
        if (isLocalPlayer)
        {
            isHitting = false;
            _imageCurseurTouche.enabled = false;
        }
    }

    public void AfficheMsgMort(bool isActive)
    {
        if (isLocalPlayer)
        {
            _hud.alpha = isActive ? 0.0f :1.0f;
            _textMort.enabled = isActive;
        }
    }

    public void AddThreat(NetworkInstanceId netID)
    {
        if (isLocalPlayer)
        {
            if (!lNetIdEnnemi.Contains(netID))
            {
                lNetIdEnnemi.Add(netID);
                CursorDmg cdmg;
                cdmg._anchorCursor = (GameObject)Instantiate(CursorDamage);
                cdmg._anchorCursor.transform.position = centerOfCanvas;
                cdmg._anchorCursor.transform.SetParent(myCanvas.transform);
                cdmg._timeToLive = Time.time;
                lCursorDmg.Add(cdmg);
                Debug.Log(lNetIdEnnemi.Count);
                RefreshThreatTTL();
            }
            else
            {
                CursorDmg cdmg;
                cdmg = lCursorDmg[lNetIdEnnemi.IndexOf(netID)];
                cdmg._timeToLive = Time.time;
                lCursorDmg[lNetIdEnnemi.IndexOf(netID)] = cdmg;
            }
        }
        
    }

    private void RefreshThreatTTL()
    {
        if (isLocalPlayer)
        {
            Vector3 posThreat;
            Matrix4x4 WorldToCamMat = myCamera.worldToCameraMatrix;
            Vector4 posThreatInCamCoor;
            float angle;
            foreach (NetworkInstanceId idThreat in lNetIdEnnemi)
            {
                posThreat = ClientScene.FindLocalObject(idThreat).transform.position;
                Matrix4x4 nMat = Matrix4x4.TRS(posThreat, myCamera.transform.rotation, new Vector3(1, 1, -1));
                //posThreat = WorldToCamMat * posThreat;
                //Debug.Log("Pos en coor cam : " + posThreat);
                posThreatInCamCoor = new Vector4(posThreat.x, posThreat.y, posThreat.z, 1.0f);
                posThreatInCamCoor = WorldToCamMat * posThreatInCamCoor;

                posThreatInCamCoor.z = 0.0f;
                posThreatInCamCoor.Normalize();

                // on deduit l'angle pour affichage UI
                angle = Vector3.Angle(posThreatInCamCoor, Vector3.up);
                CursorDmg newCdmg = lCursorDmg[lNetIdEnnemi.IndexOf(idThreat)];
                newCdmg._anchorCursor.transform.rotation = Quaternion.identity;
                newCdmg._anchorCursor.transform.Rotate(Vector3.forward, posThreatInCamCoor.x > 0 ? -angle : angle);
                // changement de l'alpha
                angle = (Time.time - newCdmg._timeToLive) / timeMaxCursorDmg;
                posThreatInCamCoor = newCdmg._anchorCursor.transform.Find("CursorDmg").GetComponent<Image>().color;
                posThreatInCamCoor.w = 1.0f - angle;
                newCdmg._anchorCursor.transform.Find("CursorDmg").GetComponent<Image>().color = posThreatInCamCoor;
                lCursorDmg[lNetIdEnnemi.IndexOf(idThreat)] = newCdmg;
            }


            for(int i = lNetIdEnnemi.Count - 1; i>=0;i--)
            {
                if(Time.time - lCursorDmg[i]._timeToLive > timeMaxCursorDmg)
                {
                    Destroy(lCursorDmg[i]._anchorCursor.gameObject);
                    lCursorDmg.RemoveAt(i);
                    lNetIdEnnemi.RemoveAt(i);
                }
            }

        }
    }

    public void ClearThreatTTL()
    {
        for (int i = lNetIdEnnemi.Count - 1; i >= 0; i--)
        {
                Destroy(lCursorDmg[i]._anchorCursor.gameObject);
                lCursorDmg.RemoveAt(i);
                lNetIdEnnemi.RemoveAt(i);
        }
    }

    public void PutAlphaActive(WeaponsManager.WeaponName wpName)
    {
        PutAlphaInactive();

        switch (wpName)
        {
            case WeaponsManager.WeaponName.Mitrailleuse:
                    _imageBarreMitrailleuse.color = COLOR_SELECTED;
                break;
            case WeaponsManager.WeaponName.FusilPompe:
                    _imageBarrePompe.color = COLOR_SELECTED;
                break;
            case WeaponsManager.WeaponName.OneShooter:
                    _imageBarreOneShooter.color = COLOR_SELECTED;
                break;
            case WeaponsManager.WeaponName.Bazooka:
                    _imageBarreBazooka.color = COLOR_SELECTED;
                break;
        }
    }

    private void PutAlphaInactive()
    {
            _imageBarreMitrailleuse.color = COLOR_MITRA;
            _imageBarrePompe.color = COLOR_POMPE;
            _imageBarreOneShooter.color = COLOR_OS;
            _imageBarreBazooka.color = COLOR_BAZOOKA;
    }


    // active le blood
    private void AfficheBlood(float valLife)
    {
        if(valLife < 1.0f)
        {
            isBlooding = true;
            _imageBlood.enabled = true;
            colorBlood.a = 1.0f;
            _imageBlood.color = colorBlood;
            timerBlood = Time.time;
        }
    }

    // Systeme affichage 
    [ClientRpc]
    public void RpcGameManager(int typeMessage, int minutesDeJeu)
    {


        if(typeMessage == 0) // en attente de connexion
        {                   // minute de jeu vaut numJoueur
            tmpString = "En attente de ";
            tmpString += (minutesDeJeu).ToString();
            tmpString += " joueur";
            tmpString += minutesDeJeu > 2 ? "" : "s";
            _txtMessageGameManager.text = tmpString;
        }
        else if (typeMessage == 1)// decompte debut combat
        {
            tmpString = "Debut des combats dans ";
            tmpString += minutesDeJeu.ToString();
            tmpString += " seconde" + (minutesDeJeu > 1 ? "s" : "");
            _txtMessageGameManager.text = tmpString;
        }
        else if (typeMessage == 2)
        {
            tmpString = "Combattez !";
            _txtMessageGameManager.text = tmpString;
        }
        else if (typeMessage == 3)
        {
            _txtMessageGameManager.text = "";
        }
        else if (typeMessage == 4)
        {
            tmpString = (Mathf.FloorToInt(minutesDeJeu / 60.0f)).ToString();
            tmpString += ":";
            if (((minutesDeJeu - (Mathf.FloorToInt(minutesDeJeu / 60.0f) * 60)) % 60) < 10)
            {
                tmpString += "0";
            }
            tmpString += ((minutesDeJeu - (Mathf.FloorToInt(minutesDeJeu / 60.0f) * 60)) % 60).ToString();
            _chronoPartie.text = tmpString;
        }
        else if (typeMessage == 5)
        {
            tmpString = "Combat terminé";
            _txtMessageGameManager.text = tmpString;
            _canvasTabScore.enabled = true;
            GameObject.Find("KillReportManager").GetComponent<KillReportManager>().MajTableauScore();
            _BtnRetourMenu.SetActive(true);
            Cursor.visible = true;
        }
        
    }

    void OnDestroy()
    {
        BoostPack.OnChangeNbBoost -= MajBoost;
        BoostPack.OnChangeTimeBoost -= ActiveBoost;
        Player.OnChangeLife -= MajLife;
        Player.OnChangeLife -= AfficheBlood;
        Player.OnChangeDeath -= AfficheMsgMort;
        Bazooka.OnChangeNbMun -= MajBazooka;
        Mitrailleuse.OnChangeNbMun -= MajMitrailleuse;
        OneShooter.OnChangeNbMun -= MajOneShooter;
        FusilPompe.OnChangeNbMun -= MajPompe;
        Weapon.OnHitSometing -= Hit;
        WeaponsManager.OnChangeSelectedWeapon -= PutAlphaActive;
    }
}
