﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerNetworkSetup : NetworkBehaviour {

    [SerializeField]
    Camera MyCam;
    [SerializeField]
    AudioListener myAudioListerner;

	// Use this for initialization
	void Start () {

        if (isLocalPlayer)
        {
            GameObject.Find("MainCamera").SetActive(false);
            MyCam.enabled = true;
            myAudioListerner.enabled = true;
            
        }
        else
        {
            MyCam.enabled = false;
            myAudioListerner.enabled = false;
        }
	}

}
