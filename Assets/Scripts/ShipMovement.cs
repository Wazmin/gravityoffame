﻿using UnityEngine;
using UnityEngine.Networking;

using System.Collections.Generic;

public class ShipMovement : NetworkBehaviour {
    public float seuilBruitPas;
    public const float SPEED_BASE = 8.0f;
    public PlayerAnimations myPlayerAnimation;
    public float intensiteJoyManette;

    public GameObject Camera;

    //reference to the changeGravity script
    public GravityPack gP;
    public Player myPlayer;
    private WeaponsManager myWeaponsManager;
    private Transform cameraTransform;
    public AudioSource aS;
    public AudioClip footStep;


    public float m_SpeedHautBas = 2.0f;         // vitesse de deplacement avant/arriere
    public float m_SpeedGaucheDroite = 2.0f;
    public float m_X_Axis = 2.0f;              // vitesse d'inclinaison haut/bas
    public float m_Y_Axis = 2.0f;              // vitesse de rotation droite/gauche

    // rigidbody de reference 
    public Rigidbody m_Rigidbody;
    Vector3 changeVelocity;
    Vector3 rbVelocity;
    float sourisY;
    float sourisX;
    Vector3 localVelocity;
    // reaction quand touche mur
    bool isTouchingWall=false;

    List<Vector3> lVecteurNormal = new List<Vector3>();

    Vector3 normaleMur;
    public float collisionCheckDistance;
    Vector3 directionToMove;
    //vector rotation de reference souris
    Vector3 sourisRotation;

    /* --- VARIABLE DES INPUTS --- */
    private float m_HorizontalInputValue;
    private float m_VerticalInputValue;
    private float m_MousseXInputValue;
    private float m_MousseYInputValue;

    float timer;

    public float speed = 8.0f;
    public float maxVelocityChange = 10.0f;
    public float maxVelocityChangeSide = 5.0f;
    public float maxVelocityChangeBack = 7.5f;

    uint compteur = 0;

    float horizontalVelocity;
    float verticalVelocity;
    bool isFalling;
    float inclinaisonTmp;

    public override void OnStartLocalPlayer()
    {
        // cacher le HUD Network
        GameObject.Find("InterfaceArene").SetActive(false);
        if(GameObject.Find("MainCamera") != null)
        GameObject.Find("MainCamera").SetActive(false);
        //tranform
        aS.enabled = true;
        // PlayerCamera
        transform.Find("PlayerCamera").transform.GetComponent<Camera>().enabled = true;
        transform.Find("PlayerCamera").transform.GetComponent<AudioListener>().enabled = true;

        //FakeAmre
        transform.Find("PlayerCamera/PackArme").transform.GetComponent<AudioSource>().enabled = true;
       
        cameraTransform = transform.Find("PlayerCamera");
        timer = Time.time;
        m_Rigidbody = GetComponent<Rigidbody>();
        
        myPlayer = GetComponent<Player>();
        if (myPlayer.controlManette)
        {
            m_X_Axis = 150.0f;              // vitesse d'inclinaison haut/bas
            m_Y_Axis = 150.0f;
        }


        myWeaponsManager = GetComponent<WeaponsManager>();
        inclinaisonTmp = 0;
    }

	
	// Update is called once per frame
	void Update () {
        localVelocity = transform.InverseTransformVector(m_Rigidbody.velocity);
        horizontalVelocity = localVelocity.x / 16.0f;
        verticalVelocity =  (Mathf.Clamp(localVelocity.z / 16.0f,-0.5f,0.5f));
        if (localVelocity.y < -2.0f) isFalling =true;
       else isFalling =false;

        // inclinaison pour blend anim
        inclinaisonTmp = Vector3.Angle(transform.up, Camera.transform.forward);
        inclinaisonTmp = (((inclinaisonTmp - 90) / 70) + 1) / 2;

        myPlayerAnimation.SetHorizontalSpeed(horizontalVelocity);
        myPlayerAnimation.SetVerticalSpeed(verticalVelocity);
        myPlayerAnimation.SetIsFalling(isFalling);
        myPlayerAnimation.SetInclinaison(inclinaisonTmp);

    }

    void FixedUpdate()
    {
        if (!isLocalPlayer) return;
        if (myPlayer.frozen) return;

        m_HorizontalInputValue = Input.GetAxis("Horizontal");
        m_VerticalInputValue = Input.GetAxis("Vertical");

        if (myPlayer.controlManette)
        {
            m_MousseXInputValue =   Input.GetAxis("RJoy_X");
            m_MousseYInputValue = Input.GetAxis("RJoy_Y");

            m_MousseXInputValue *= m_MousseXInputValue >0 ? m_MousseXInputValue  : -m_MousseXInputValue ;
            m_MousseYInputValue *= m_MousseYInputValue >0 ? m_MousseYInputValue  : -m_MousseYInputValue ;

            m_MousseXInputValue *= myWeaponsManager.GetWeaponAgilityMovement();
            m_MousseYInputValue *= myWeaponsManager.GetWeaponAgilityMovement();
           
            
            
        }
        else
        {
            m_MousseXInputValue = Input.GetAxis("Mouse X");
            m_MousseYInputValue = Input.GetAxis("Mouse Y");
        }

        if (!Input.GetKey(KeyCode.LeftShift) && !myPlayer.isDead)
        {
            MouvementsDroits();
        }

        if (!gP.isChangingGravity)
        {
            MouvementsRotations();
        }

        

    }

    //correspond aux deplacements du personnage
    void MouvementsDroits()
    {
        if (!gP.grounded)
        {
            directionToMove = new Vector3(m_HorizontalInputValue*0.8f, 0, m_VerticalInputValue * 0.8f);
        }
        else
        {
            directionToMove = new Vector3(m_HorizontalInputValue, 0, m_VerticalInputValue);
        }
        


        directionToMove = transform.TransformVector(directionToMove);
        directionToMove *= speed;
        if ((m_Rigidbody.velocity.magnitude > seuilBruitPas) && gP.grounded)
        {
            if ((Time.time - timer) > 0.3)
            {
                timer = Time.time;
                CmdPlayFootStep();
            }
        }

        rbVelocity = m_Rigidbody.velocity;
        // on retire la velocity dans le sens y du joueur
        rbVelocity = transform.InverseTransformVector(rbVelocity);
        rbVelocity.y = 0;
        rbVelocity = transform.TransformVector(rbVelocity);

        changeVelocity = directionToMove - rbVelocity;
        changeVelocity = transform.InverseTransformVector(changeVelocity);
        changeVelocity.x = Mathf.Clamp(changeVelocity.x, -maxVelocityChangeSide, maxVelocityChangeSide);
        changeVelocity.z = Mathf.Clamp(changeVelocity.z, -maxVelocityChangeBack, maxVelocityChange);
        changeVelocity.y = 0;
        changeVelocity = transform.TransformVector(changeVelocity);

        m_Rigidbody.AddForce(changeVelocity, ForceMode.Impulse);

        localVelocity = transform.InverseTransformVector(m_Rigidbody.velocity);


    }


    // correspond aux depalcements de la camera
    void MouvementsRotations()
    {

        sourisY = m_Y_Axis * m_MousseYInputValue * Time.deltaTime;
        sourisX = m_X_Axis * m_MousseXInputValue * Time.deltaTime;

        transform.Rotate(Vector3.up, sourisX);
        cameraTransform.transform.Rotate(Vector3.right, -sourisY);

    }

    // fonction pour faire des moouvements proportionnels à l'intensité de l'inclinaison
    void InitPlayerMovements()
    {
        speed = SPEED_BASE;
    }

    public void StopMovement()
    {
        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.angularVelocity = Vector3.zero;
    }

    [ClientRpc]
    public void RpcStopMovements()
    {
        StopMovement();
    }

   [Command]
   void CmdPlayFootStep()
    {
        RpcPlayFootStep();
    }

    [ClientRpc]
    void RpcPlayFootStep()
    {
        //float tmpPitch = Random.Range(0.0f, 0.2f);
        //aS.pitch = 0.8f + tmpPitch;
        aS.PlayOneShot(footStep, 0.25f);

    }
}
