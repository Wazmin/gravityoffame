﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;
using System;

public class Pistolet : Weapon {

    public GameObject effetTirPistolet;
    public float usage;
    public float maxUsage;
    public float cranUsage;
    public float cranRefroidissement;
    public float tempsIndispo;
    public bool enUsage;
    public bool froide;
    public bool surchauffe;

    public override void OnStartLocalPlayer()
    {
        timerTirPrincipal = Time.time;
        timerZoom = Time.time;
        froide = true;
        surchauffe = false;
        idPlayer = GetComponent<NetworkIdentity>().netId;
        nbChargeEnCoursTxt = GameObject.Find("Canvas/HUD/nbChargeEnCours").GetComponent<Text>();
    }

    void Start()
    {
        if (!isLocalPlayer) return;
    }

    void Update()
    {
        if (!isLocalPlayer) return;
        if (surchauffe)
        {
            if (Time.time - timerRechargement > tempsIndispo)
            {
                surchauffe = false;
                usage -= cranRefroidissement;
            }
                
        }
        else
        {
            froide = (usage == 0);
            if (usage >= maxUsage)
            {
                surchauffe = true;
                CmdAnimTir(false);
                timerRechargement = Time.time;
            }
            if (!enUsage && !froide)
            {
                if (Time.time - timerRechargement > tempsRechargement)
                {
                    timerRechargement = Time.time;
                    usage -= cranRefroidissement;
                    if (usage <= 0.0f)
                    {
                        usage = 0.0f;
                        froide = true;
                    }
                }
            }
        }
        if(isActiveWeapon)
        nbChargeEnCoursTxt.text = (usage/maxUsage *100).ToString("F0") + "%";
    }

   
    //fonction tirer en raycast
    public override void Tirer(string shooterName)
    {
        enUsage = true;
        if (Time.time - timerTirPrincipal >= frequenceTir * myWeaponsManager.tauxVitesseCadenceTir && !surchauffe)
        {
            timerTirPrincipal = Time.time;
            CmdTir(nbDegatPrincipal, nomArme, shooterName, idPlayer);
            usage += cranUsage;
            if (usage > maxUsage) usage = maxUsage;
        }
    }

    [Command]
    void CmdTir(float pDegats, string pNomArme,string pShooterName, NetworkInstanceId _idPlayer)
    {
        bool touche = false;
        Ray myRay = new Ray(tCam.position, tCam.forward);
        // reference au joueur qui tir
        Player shooterPlayer = NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>();
        RaycastHit[] hits;
        hits = Physics.RaycastAll(myRay, 100.0f).OrderBy(h => h.distance).ToArray();
        int i = 0;
        int maxI = hits.Count();
        while(i < maxI && !touche)
        {
            if (hits[i].collider.tag != "JambesPlayer" && hits[i].collider.tag != "TorsePlayer"
                && hits[i].collider.tag != "HeadShot" && hits[i].collider.tag != "MainCamera"
                && hits[i].collider.tag != "NotToDamage" && hits[i].collider.tag != "Player")
            {
                i = 100000; //on sort si on collide un truc qui n'est pas un joueur
            }
            else
            {
                //touche jambe ou torse sauf le sien
                if ((hits[i].collider.tag == "JambesPlayer"
                    || hits[i].collider.tag == "TorsePlayer")
                    && hits[i].collider != shooterPlayer.torseHitBox
                    && hits[i].collider != shooterPlayer.jambesHitBox)
                {
                    hits[i].transform.GetComponent<Player>().TakeDamages(pDegats, nomArme, pShooterName, _idPlayer);
                    touche = true;
                }

                if (hits[i].collider.tag == "HeadShot"
                   && hits[i].collider != shooterPlayer.headHitBox)
                {
                    hits[i].transform.GetComponent<Player>().
                        TakeDamages(pDegats * myWeaponsManager.bonusDegatHeadShot, nomArme, pShooterName, _idPlayer);
                    touche = true;
                    NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>().RpcPlayHeadShotSound();
                }
            }
            i++;
        }

        RpcEffetTir(touche);
        RpcAnimTir(true);
    }

    public override void RelacheTirer(string shooterName)
    {
        enUsage = false;
        CmdAnimTir(false);
    }

    
    public override void MajInfoWeapon()
    {      
        nbChargeEnCoursTxt.text = "0%";
    }

    public override void DisableWeapon()
    {
        isActiveWeapon = false;
        isReloading = false;
        isZooming = false;
        enUsage = false;
        playerCamera.fieldOfView = FIELD_OF_VIEW;
        if (!isServer)
        {
            CmdAnimTir(false);
        }
        else
        {
            RpcAnimTir(false);
        }
        

    }

    // instanciate local des effets
    [ClientRpc]
    void RpcEffetTir(bool touche)
    {
        GameObject effetBalle;
        if (isLocalPlayer)
        {
            effetBalle = (GameObject)Instantiate(effetTirPistolet, canonFPS.position, tCam.rotation);
        }
        else
        {
            effetBalle = (GameObject)Instantiate(effetTirPistolet, canonExt.position, tCam.rotation);
        }

        aS.PlayOneShot(sonTirPrincipal, 0.7f);
        Destroy(effetBalle, 3.0f);

        if (isLocalPlayer && touche)
        {
            hitEvent();
        }
    }

    [Command]
    public void CmdAnimTir(bool b)
    {
        RpcAnimTir(b);
    }

    // anim de tir
    [ClientRpc]
    public void RpcAnimTir(bool isFiring)
    {
        myWeaponsManager.myPlayerAnimation.IsFiring(isFiring);
    }

    
    // anim de rechargement
    [ClientRpc]
    public void RpcAnimReload()
    {
        myWeaponsManager.myPlayerAnimation.Recharger();
    }
}
