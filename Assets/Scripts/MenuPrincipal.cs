﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuPrincipal : MonoBehaviour {
    public Button boutonArene;
    public Button boutonControles;
    public Button boutonCredit;
    public Button boutonQuitter;
    public Canvas MenuQuitter;
    private bool isMenuQuitter;
    public Button boutonFuir;
    public Button boutonRester;

    public bool isMenuPrincipal;
    public Canvas EcranChargement;
    public GameObject buttonRetour;
    public Image ManetteMapping;
    public Image ClavierMapping;
    public Image BarreChargementExt;
    public Image BarreChargement;
    public Text TextChargement;
    public Text BtnText;

    public Canvas EcranCredit;

    private bool isClavier;

    public AudioClip sonFouet;
    public AudioClip sonBayonette;
    public AudioClip sonHou;
    private AudioSource AS;

	// Use this for initialization
	void Start () {
        isMenuQuitter = false;
        isMenuPrincipal = true;
        AS = GameObject.Find("Musique").GetComponent<AudioSource>();
        isClavier = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("escape") && isMenuPrincipal)
            ActiveSousMenuQuitter();

    }

    public void PlayButtonSound()
    {
        AS.PlayOneShot(sonBayonette);
    }

    public void ActiveSousMenuQuitter()
    {
        isMenuQuitter = !isMenuQuitter;
        if (isMenuQuitter) AS.PlayOneShot(sonFouet);
        ActiveButton(!isMenuQuitter);
        MenuQuitter.enabled = isMenuQuitter;
    }

    void ActiveButton(bool b)
    {
        boutonArene.interactable = b;
        boutonControles.interactable = b;
        boutonCredit.interactable = b;
        boutonQuitter.interactable = b;
    }

    public void QuitterJeu()
    {
        AS.PlayOneShot(sonHou);
        Invoke("Tempo", 3.3f);
    }
    void Tempo()
    {
        Application.Quit();
    }

    public void EntrerDansLArene()
    {
        AS.PlayOneShot(sonBayonette);
        StartCoroutine(LevelCoroutine());
    }

    // pour le chargement entre les ecrans
    IEnumerator LevelCoroutine()
    {
        ActiveButton(false);
        EcranChargement.enabled = true;
        buttonRetour.SetActive(false);
        ManetteMapping.enabled = true;
        BarreChargementExt.enabled = true;
        BarreChargement.enabled = true;
        TextChargement.enabled = true;
        
        AsyncOperation async = SceneManager.LoadSceneAsync("mainScene");
        while (!async.isDone)
        {
            BarreChargement.fillAmount = async.progress / 1.0f;
            yield return null;
        }
     }

    public void ControlesEtArsenal()
    {
        AS.PlayOneShot(sonBayonette);
        isMenuPrincipal = false;
        ActiveButton(false);
        EcranChargement.enabled = true;
        buttonRetour.SetActive(true);
    }

    public void OnClicSwitchButton()
    {
        AS.PlayOneShot(sonBayonette);
        if (!isClavier)
        {
            isClavier = true;
            ClavierMapping.enabled = true;
            ManetteMapping.enabled = false;
            BtnText.text = "Controles Manette";
        }else
        {
            isClavier = false;
            ManetteMapping.enabled = true;
            ClavierMapping.enabled = false;
            BtnText.text = "Controles Clavier";
        }
    }

    public void RetourMenuPrincipal()
    {
        AS.PlayOneShot(sonBayonette);
        ActiveButton(true);
        isMenuPrincipal = true;
        EcranChargement.enabled = false;
        EcranCredit.enabled = false;
    }

    public void OnClicEcranCredit()
    {
        AS.PlayOneShot(sonBayonette);
        isMenuPrincipal = false;
        ActiveButton(false);
        EcranCredit.enabled = true;
    }
   
}
