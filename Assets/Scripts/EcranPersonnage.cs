﻿using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class EcranPersonnage : MonoBehaviour {
    private Text txtNomJoueur;
    private GameObject pnlBlocPseudo;
    private GameObject txtDecompte;
    private Toggle toggleManette;
    private AudioSource aS;
    public AudioClip sonValide;
    private float timer;
    public int tempDecompte;
    private bool startDecompte = false;
    public bool controlManette;

    // Use this for initialization
    void Start () {
        txtNomJoueur = GameObject.Find("Canvas/BlocPseudo/InputField/Text").GetComponent<Text>();
        pnlBlocPseudo = GameObject.Find("Canvas/BlocPseudo");
        txtDecompte = GameObject.Find("Canvas/TxtDecompte");
        txtDecompte.SetActive(false);
        aS = GameObject.Find("Musique Ambiance").GetComponent<AudioSource>();
        DontDestroyOnLoad(GameObject.Find("Musique Ambiance"));
        toggleManette = GameObject.Find("Canvas/BlocPseudo/Toggle").GetComponent<Toggle>(); ;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Y))
        {
            
        }
        if (startDecompte)
        {
            Decompte();
        }
    }


    public void SettingNamePlayer()
    {
        if (txtNomJoueur.text.Length<1) return;
        GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().nomGladiateur = txtNomJoueur.text;
        GameObject.Find("InfoJoueur").GetComponent<InfoJoueur>().controlManette = toggleManette.isOn;
        pnlBlocPseudo.SetActive(false);
        txtDecompte.SetActive(true);
        txtDecompte.GetComponent<Text>().text = tempDecompte.ToString();
        startDecompte = true;
        aS.PlayOneShot(sonValide,0.5f);
        timer = Time.time;
    }

    void Decompte()
    {
        if(Time.time - timer >= 1.0f )
        {
            txtDecompte.GetComponent<Text>().text = (--tempDecompte).ToString();
            timer = Time.time;
        }
        if(tempDecompte <= 0)
        {
            startDecompte = false;
            // chargement lobby
            SceneManager.LoadScene("mainScene");
        }
    }
}
