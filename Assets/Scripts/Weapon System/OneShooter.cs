﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Linq;
using System.Collections;
using System;

/**
 *  Tout est calculé autour de l'unité d'une charge 
 **/

public class OneShooter : Weapon {
    public Image barreCharge;
    // Events
    public delegate void MajUIMunition(float _charge);
    public static event MajUIMunition OnChangeNbMun;

    public GameObject effetRayonOS;
    public AudioSource aSLoopSound;
    public AudioSource aSLoopSoundExt;
    public AudioClip sonChargement;
    private float timerStartCharge;
    public bool isCharging;
    public float qteEnergieRestCharge;
    public float tempsMaxCharge = 3.0f;
    public int nbMunConsoMinimal = 3;
    public float rayonRaycast;
    private float fVibrationManette;

    private bool isFullCharged;

    public override void OnStartLocalPlayer()
    {
        timerTirPrincipal = Time.time;
        timerZoom = Time.time;
        idPlayer = GetComponent<NetworkIdentity>().netId;
        timerStartCharge = -1.0f;
        nbChargeEnCoursTxt = GameObject.Find("Canvas/HUD/nbChargeEnCours").GetComponent<Text>();
        fVibrationManette = 0.0f;
    }

    void Start()
    {
        if (!isLocalPlayer) return;
    }

    public override void SpecialUpdateAction()
    {
        if (isLocalPlayer)
        {
            if (isCharging)
            {
                if (myWeaponsManager.myPlayer.controlManette)
                {
                    fVibrationManette = Mathf.Min((Time.time - timerStartCharge) / tempsMaxCharge, 1.0f);
                    Utils.Import.XInputGamePadSetState(0, 0.0f, fVibrationManette);
                }
                barreCharge.fillAmount = Mathf.Min(((Time.time - timerStartCharge)/tempsMaxCharge),1.0f);
            }      
        }
    }


    public override void Tirer(string shooterName)
    {
        if (!isReloading)
        {
            if (nbChargeEnCours <= 0 && qteEnergieRestCharge <= 0.0f)
            {
                if (myWeaponsManager.nbMunitions <= 0)
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    myWeaponsManager.CmdSetWeapon(WeaponsManager.WeaponName.Pistolet);
                }
                else
                {
                    Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
                    TempoRecharger();
                }
            }
            else
            {
                if (!isCharging)
                {
                    timerStartCharge = Time.time;
                    isCharging = true;
                    CmdPlayChargingSound(myWeaponsManager.tauxVitesseCadenceTir);
                }
                else
                {
                    if (Time.time - timerStartCharge > (tempsMaxCharge * myWeaponsManager.tauxVitesseCadenceTir) )
                    {
                        CmdLoopChargedSound();
                    }
                }
            }
        }
        else
        {

        }
       
    }


    public override void RelacheTirer(string shooterName)
    {
        if (isCharging)
        {
            timerStartCharge = Mathf.Min(Time.time - timerStartCharge, tempsMaxCharge * myWeaponsManager.tauxVitesseCadenceTir);
            isCharging = false;
            //partie raycast
            CmdTir(damagesDone(timerStartCharge), nomArme, shooterName,idPlayer);
            RetirerMunition(consoEnergieTir(timerStartCharge));
            Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
            barreCharge.fillAmount = 0.0f;
        }
    }

    [Command]
    void CmdTir(float pDegats, string pNomArme, string pShooterName, NetworkInstanceId _idPlayer)
    {
        //RaycastHit hit;
        bool touche = false;
        bool continuer = true;
        float tauxCharge =
        Mathf.Max(Mathf.Min(tempsMaxCharge, timerStartCharge),0.0f)/tempsMaxCharge;

        Ray myRay = new Ray(tCam.position, tCam.forward);
        // reference au joueur qui tir
        Player shooterPlayer = NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>();
        RaycastHit[] hits;
        float r = Mathf.Max(0.01f, rayonRaycast * tauxCharge);
        hits = Physics.SphereCastAll(myRay, r, 100.0f).OrderBy(h => h.distance).ToArray();
        int i = 0;
        int maxI = hits.Count();
        while (i < maxI && !touche && continuer)
        {
            if (hits[i].collider.tag != "JambesPlayer" && hits[i].collider.tag != "TorsePlayer"
                && hits[i].collider.tag != "HeadShot" && hits[i].collider.tag != "MainCamera"
                && hits[i].collider.tag != "NotToDamage" && hits[i].collider.tag != "Player")
            {
                continuer = false;
            }
            else
            {
                //touche jambe ou torse sauf le sien
                if ((hits[i].collider.tag == "JambesPlayer"
                    || hits[i].collider.tag == "TorsePlayer")
                    && hits[i].collider != shooterPlayer.torseHitBox
                    && hits[i].collider != shooterPlayer.jambesHitBox)
                {
                    hits[i].transform.GetComponent<Player>().TakeDamages(pDegats, nomArme, pShooterName, _idPlayer);
                    touche = true;
                }

                if (hits[i].collider.tag == "HeadShot"
                   && hits[i].collider != shooterPlayer.headHitBox)
                {
                    hits[i].transform.GetComponent<Player>().
                        TakeDamages(pDegats * myWeaponsManager.bonusDegatHeadShot, nomArme, pShooterName, _idPlayer);
                    touche = true;
                    NetworkServer.FindLocalObject(_idPlayer).GetComponent<Player>().RpcPlayHeadShotSound();
                }
            }
            i++;
        }
        i--;
        RpcEffetTir(hits[i].distance, touche);
        RpcAnimTrigTir();
    }

    // fonction de calcul de degat
    // avec un max par rapport au munitions restantes
    // fonction exponentielle
    private float damagesDone(float chargeTime)
    {
        float chargeMaximumPossible = (qteEnergieRestCharge + (nbChargeEnCours)) * tempsMaxCharge * myWeaponsManager.tauxVitesseCadenceTir; // -1 cause celui en cours
        chargeTime = Mathf.Min(chargeTime, chargeMaximumPossible);
        chargeMaximumPossible = (Mathf.Exp(chargeTime) * 0.5f + 1) / (Mathf.Exp(tempsMaxCharge * myWeaponsManager.tauxVitesseCadenceTir) * 0.5f + 1) * nbDegatPrincipal;
        return Mathf.Max(chargeMaximumPossible, 10.0f);
    }

    // consommation d'energie du tir
    // avec un max par rapport au munitions restantes
    // indépendance du lieu de l'appel de la fonction
    // fonction lineraire f(x) = x/xMax * NB_MUN
    private float consoEnergieTir(float chargeTime)
    {
        float chargeMaximumPossible = (qteEnergieRestCharge + (nbChargeEnCours)) * tempsMaxCharge * myWeaponsManager.tauxVitesseCadenceTir; // -1 cause celui en cours
        chargeTime = Mathf.Min(chargeTime, chargeMaximumPossible);
        return (chargeTime / tempsMaxCharge * myWeaponsManager.tauxVitesseCadenceTir * NB_MUNITIONS_PAR_CHARGE);
    } 

    // calcule les munitions a retirer
    private void RetirerMunition(float nbMunConso)
    {
        //si moins de munition que le minimum
        if (nbMunConso <= nbMunConsoMinimal)
        {
            float consoRestante = (qteEnergieRestCharge + nbChargeEnCours) * NB_MUNITIONS_PAR_CHARGE;

            //si il y a pourtant assez de munition pour le minimum alors on le consomme
            if (nbMunConsoMinimal > consoRestante)
            {
                nbMunConso = consoRestante;
            }
            else
            {
                nbMunConso = nbMunConsoMinimal;
            }

        }

        float tmpMunTotalCourante = (NB_MUNITIONS_PAR_CHARGE * (qteEnergieRestCharge + nbChargeEnCours));
        tmpMunTotalCourante -= nbMunConso;

        if (tmpMunTotalCourante <=0)
        {
            qteEnergieRestCharge = 0.0f;
            nbChargeEnCours = 0;
            TempoRecharger();
        }else
        {
            nbChargeEnCours = Mathf.FloorToInt(tmpMunTotalCourante / NB_MUNITIONS_PAR_CHARGE);
            qteEnergieRestCharge = (tmpMunTotalCourante % NB_MUNITIONS_PAR_CHARGE)/ ((float)NB_MUNITIONS_PAR_CHARGE);
        }
        
        if(nbChargeEnCours <=0 && qteEnergieRestCharge<=0 && myWeaponsManager.nbMunitions <= 0)
        {
             myWeaponsManager.CmdSetWeapon(WeaponsManager.WeaponName.Pistolet);
        }
        else if (nbChargeEnCours <= 0 && qteEnergieRestCharge <= 0)
        {
            TempoRecharger();
        }
        MajInfoWeapon();
    }

    //rechargement de l'arme
    public override void Recharger()
    {
        //on complete la charge en cours
        if(qteEnergieRestCharge < 1.0f)
        {
            int nbMunACombler = (int)((1.0f - qteEnergieRestCharge) * NB_MUNITIONS_PAR_CHARGE);
            if (nbMunACombler <= myWeaponsManager.nbMunitions)
            {
                CmdDestocker(nbMunACombler);
                qteEnergieRestCharge = 1.0f;

            }
            else
            {
                qteEnergieRestCharge += myWeaponsManager.nbMunitions / NB_MUNITIONS_PAR_CHARGE;
                //CmdDestocker(nbMunACombler);
            }
        }

        //on complete les charges completes
        //NB CHARGES - 1 car qteEnergieRestante
        while (NB_CHARGES_PAR_CHARGEUR - 1 > nbChargeEnCours && myWeaponsManager.nbMunitions >= NB_MUNITIONS_PAR_CHARGE)
        {
            CmdDestocker(NB_MUNITIONS_PAR_CHARGE);
            nbChargeEnCours++;
        }

        //petite verif parce que je le sens pas
        if (nbChargeEnCours > NB_CHARGES_PAR_CHARGEUR)
        {
            nbChargeEnCours = NB_CHARGES_PAR_CHARGEUR;
            Debug.LogError("Plus de charge que prevue dans le OneShoter");
        }
        if(qteEnergieRestCharge > 1.0f)
        {
            qteEnergieRestCharge = 1.0f;
            Debug.LogError("Plus de qte energie que prevue dans le Oneshooter");
        }
        MajInfoWeapon();
        isReloading = false;
    }

    public override void MajInfoWeapon()
    {
        if (OnChangeNbMun != null)
            OnChangeNbMun((nbChargeEnCours +qteEnergieRestCharge)/3.0f);
        nbChargeEnCoursTxt.text = (nbChargeEnCours + qteEnergieRestCharge ).ToString("F1");
    }


    public override void ResetWeapon()
    {
        nbChargeEnCours = NB_CHARGES_PAR_CHARGEUR - 1;
        qteEnergieRestCharge = 1.0f;
    }


    // override pour la loop et le charge weapon
    public override void ConsommerMunition()
    {
        isActiveWeapon = false;
        isReloading = false;
        isZooming = false;
        isCharging = false;
        playerCamera.fieldOfView = FIELD_OF_VIEW;
        CmdStopWeaponSound();
    }

    [Command]
    void CmdLoopChargedSound()
    {
        RpcLoopChargedSound();
    }

    [ClientRpc]
    void RpcLoopChargedSound()
    {
        if (isLocalPlayer)
        {
            if (!aSLoopSound.isPlaying)
            {
                aSLoopSound.Play();
            }
        }else
        {
            if (!aSLoopSoundExt.isPlaying)
            {
                aSLoopSoundExt.Play();
            }
        }
       
            
        
    }

    [Command]
    void CmdStopWeaponSound()
    {
        RpcStopWeaponSound();
    }

    [ClientRpc]
    void RpcStopWeaponSound()
    {
        if (isLocalPlayer)
        {
            if (aSLoopSound.isPlaying)
            {
                aSLoopSound.Stop();
            }
        }
        else
        {
            if (aSLoopSoundExt.isPlaying)
            {
                aSLoopSoundExt.Stop();
            }
        }
       
            
    }

    [Command]
    void CmdPlayChargingSound(float vitesseChargement)
    {
        RpcPlayChargingSound(vitesseChargement);
    }

    [ClientRpc]
    void RpcPlayChargingSound(float vitesseChargement)
    {
        if (isLocalPlayer)
        {
            aSLoopSound.pitch = 1 / vitesseChargement;
            aSLoopSound.PlayOneShot(sonChargement);
        }
        else
        {
            aSLoopSoundExt.pitch = 1 / vitesseChargement;
            aSLoopSoundExt.PlayOneShot(sonChargement);
        }

    }

    //instance local de l'effet
    [ClientRpc]
    void RpcEffetTir(float f, bool touche)
    {
        GameObject effetRayon;
        if (isLocalPlayer)
        {
            effetRayon = (GameObject)Instantiate(effetRayonOS, canonFPS.position, tCam.rotation);
            effetRayon.transform.position += (tCam.forward * (f / 2));
            Vector3 scaleDist = effetRayon.transform.localScale;
            scaleDist.y *= f / 50;
            effetRayon.transform.localScale = scaleDist;
            effetRayon.transform.Rotate(Vector3.right, 90.0f);
            Destroy(effetRayon, 2.0f);

            if (aSLoopSound.isPlaying)
            {
                aSLoopSound.Stop();
            }
            aSLoopSound.PlayOneShot(sonTirPrincipal, 0.7f);
        }
        else
        {
            effetRayon = (GameObject)Instantiate(effetRayonOS, canonExt.position, tCam.rotation);
            effetRayon.transform.position += (tCam.forward * (f / 2));
            Vector3 scaleDist = effetRayon.transform.localScale;
            scaleDist.y *= f / 50;
            effetRayon.transform.localScale = scaleDist;
            effetRayon.transform.Rotate(Vector3.right, 90.0f);
            Destroy(effetRayon, 2.0f);

            if (aSLoopSoundExt.isPlaying)
            {
                aSLoopSoundExt.Stop();
            }
            aSLoopSoundExt.PlayOneShot(sonTirPrincipal, 0.7f);
        }
        


        if (isLocalPlayer && touche)
        {
            hitEvent();
        }
    }

    public override bool CanBeUsed()
    {
        return nbChargeEnCours>0 && qteEnergieRestCharge>0;
    }

    // stop et reinit chargement
    public override void DisableWeapon()
    {
        isActiveWeapon = false;
        isReloading = false;
        isZooming = false;
        isCharging = false;
        playerCamera.fieldOfView = FIELD_OF_VIEW;
        CmdStopWeaponSound();
        //RpcStopWeaponSound();
        Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
        barreCharge.fillAmount = 0.0f;
    }
}
