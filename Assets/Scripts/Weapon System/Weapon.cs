﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using Utils;

public abstract class Weapon : NetworkBehaviour {

    protected int layerPlayer = 1 << 8;

    // Events
    public delegate void MajHitCurseur();
    public static event MajHitCurseur OnHitSometing;

    public AudioSource aS;
    public WeaponsManager myWeaponsManager;
    public Text nbChargeEnCoursTxt;
    public AudioClip sonRechargement;
    public AudioClip sonTirPrincipal;

    public const float FIELD_OF_VIEW = 60.0f;
    public const int MUNITION_PAR_CHARGEUR = 30;
    public bool isActiveWeapon;
    public Transform tCam;
    public Camera playerCamera;
    public Transform canonFPS;
    public Transform canonExt;
    public string nomArme;
    public float nbDegatPrincipal;
    public int NB_MUNITIONS_PAR_CHARGE;
    public int NB_CHARGES_PAR_CHARGEUR;    // 30 Mit, 10 Pomp, 3 OS, 1 Bazooka 
    public int nbChargeEnCours;
    public float timerTirPrincipal;
    public bool jeTire;
    public bool isReloading;
    public float timerRechargement;
    public float tempsRechargement;
    public float frequenceTir;
    public bool isZooming;
    public float fieldOfViewWhenZooming;
    public float indiceDeviation;
    public float MAX_DEVIATION;
    public float normalAgiliteMouvement;
    public float zoomingAgiliteMouvement;
    protected NetworkInstanceId idPlayer;

    protected float timerZoom;
    public float ticTimerZoom;
    public float cranTicZoom;


    void Update()
    {
        if(isActiveWeapon)
        {
            MakeZoom();
            if(isReloading)
            TempoRecharger();
            SpecialUpdateAction();
        }
    }

    public virtual void SpecialUpdateAction()
    {

    }

    public void ActiveWeapon()
    {
        isActiveWeapon = true;
    }

    // en cas de changement d'arme cesse le rechargement
    public virtual void DisableWeapon()
    {
        isActiveWeapon = false;
        isReloading = false;
        isZooming = false;
        Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
        playerCamera.fieldOfView = FIELD_OF_VIEW;
        if (aS.isPlaying) aS.Stop();
    }


    public virtual void TempoRecharger()
    {
        if (myWeaponsManager.nbMunitions < NB_MUNITIONS_PAR_CHARGE
                || nbChargeEnCours >= NB_CHARGES_PAR_CHARGEUR)
            return;


        if (!isReloading)
        {
            timerRechargement = Time.time;
            isReloading = true;
            aS.pitch = 1.0f;
            aS.PlayOneShot(sonRechargement);
            CmdAnimTir(false);
            jeTire = false;
           CmdAnimReload(true);
        }
        else if (Time.time - timerRechargement >=tempsRechargement * myWeaponsManager.tauxVitesseRechargement)
        {
            CmdAnimReload(false);
            Recharger();
        }
    }

    //rechargement de l'arme
    public virtual void Recharger()
    {
        
        //rechargement par bloc de charge
        while ((myWeaponsManager.nbMunitions >= NB_MUNITIONS_PAR_CHARGE) && (nbChargeEnCours < NB_CHARGES_PAR_CHARGEUR))
        {
            CmdDestocker(NB_MUNITIONS_PAR_CHARGE);
            nbChargeEnCours++;
        }
        //nbChargeEnCoursTxt.text = nbChargeEnCours.ToString();
        MajInfoWeapon();
        isReloading = false;
    }
    
    [Command]
    protected void CmdDestocker(int nbRetrait)
    {
        myWeaponsManager = GetComponent<WeaponsManager>();
        if (nbRetrait < 0) nbRetrait = 0;
        myWeaponsManager.Destocker(nbRetrait);
    } 


    // Zoom de l'arme en cours
    public void Zooming(bool order)
    {
        if (isZooming == order) return;

        if (order)
        {
            // FOV CAMERA A CHANGER
            isZooming = true;
        }
        else
        {
            // FOV CAMERA A REMETTRE
            isZooming = false;
        }        
    }

    //fonction pour smooth le zoom
    private void MakeZoom()
    {
        // cas de retour si valeur cible deja atteinte
        if (isZooming && playerCamera.fieldOfView == fieldOfViewWhenZooming) return;
        if (!isZooming && playerCamera.fieldOfView == FIELD_OF_VIEW) return;
        float tmpFOV = playerCamera.fieldOfView;

        if (Time.time - timerZoom > ticTimerZoom)
        {
            if (isZooming)//reduction du FOV
            {
                tmpFOV -= cranTicZoom;
            }
            else //augementation du FOV
            {
                tmpFOV += cranTicZoom;
            }
            timerZoom = Time.time;
        }
        if (tmpFOV > FIELD_OF_VIEW) tmpFOV = FIELD_OF_VIEW;
        else if (tmpFOV < fieldOfViewWhenZooming) tmpFOV = fieldOfViewWhenZooming;
        playerCamera.fieldOfView = tmpFOV;

    }


    public virtual void ConsommerMunition()
    {
        if (nbChargeEnCours > 0)
        {
            nbChargeEnCours--;
        }
        else
        {
            TempoRecharger();
        }
        MajInfoWeapon();
    }

    // fonction tirer abstract
    public abstract void Tirer(string shooterName);
    // fonction relache pas toujours utilise
    public abstract void RelacheTirer(string shooterName);

    public virtual void MajInfoWeapon()
    {
        nbChargeEnCoursTxt.text = nbChargeEnCours.ToString("F2");
    }

    public virtual void ResetWeapon()
    {
        nbChargeEnCours = NB_CHARGES_PAR_CHARGEUR;
    }

    protected void hitEvent()
    {
        if (!isLocalPlayer) return;
        if (OnHitSometing != null)
            OnHitSometing();
    }

    public virtual bool CanBeUsed()
    {
        return nbChargeEnCours > 0;
    }


    // Anim Tir via IsFiring
    [Command]
    public void CmdAnimTir(bool b)
    {
        RpcAnimTir(b);
    }

    [ClientRpc]
    public void RpcAnimTir(bool b)
    {
        myWeaponsManager.myPlayerAnimation.IsFiring(b);
    }

    [Command]
    public void CmdAnimTrigTir()
    {
        RpcAnimTrigTir();
    }

    [ClientRpc]
    public void RpcAnimTrigTir()
    {
        myWeaponsManager.myPlayerAnimation.Tirer();
    }

    // Anim Reload
    [Command]
    public void CmdAnimReload(bool b)
    {
        RpcAnimReload(b);
    } 
    [ClientRpc]
    public void RpcAnimReload(bool b)
    {
        myWeaponsManager.myPlayerAnimation.SetReload(b);
    }
  
}
