﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;

public class WeaponsManager : NetworkBehaviour {
    public delegate void ChangeWeapon(WeaponName _wpName);
    public static event ChangeWeapon OnChangeSelectedWeapon;

    public PlayerAnimations myPlayerAnimation;

    public bool isActive;
    private string playerName;
    public Player myPlayer;
    public float bonusDegatHeadShot;
    public enum WeaponName : int { Pistolet = 0, Mitrailleuse = 1, FusilPompe = 2, OneShooter = 3, Bazooka = 4 };

    public const int NB_WEAPONS = 5;
    public const int munitionsInitiales = 300;
    public float tauxVitesseRechargement;
    public float tauxVitesseCadenceTir;

    public GameObject[] model3DFPS = new GameObject[NB_WEAPONS];
    public GameObject[] model3DExt = new GameObject[NB_WEAPONS];

    private float timerChangementArme;
    public float freqChangementArme = 0.5f;
    private bool boutonTirRelache = true;
    public bool boutonZoomRelache = true;

    [SyncVar(hook = "OnChangeMunition")]
    public int nbMunitions = munitionsInitiales;

    public Text stockMuniTxt;

    public Weapon[] mesArmes = new Weapon[NB_WEAPONS];

    [SyncVar(hook = "OnChangeWeapon")]
    int currentWeapon =(int) WeaponName.Mitrailleuse;

    public AudioSource aS;
    public AudioClip sonPrisePackMuni;

    public override void OnStartLocalPlayer()
    {
        playerName = transform.GetComponent<Player>().pseudo;
        stockMuniTxt = GameObject.Find("Canvas/HUD/StockMuni").transform.GetComponent<Text>();
        tauxVitesseCadenceTir = 1.0f;
        tauxVitesseRechargement = 1.0f;
    }


    // Use this for initialization
    void Start () {
        mesArmes[0] = GetComponent<Pistolet>();
        mesArmes[1] = GetComponent<Mitrailleuse>();
        mesArmes[2] = GetComponent<FusilPompe>();
        mesArmes[3] = GetComponent<OneShooter>();
        mesArmes[4] = GetComponent<Bazooka>();
        if (isLocalPlayer)
        {
            timerChangementArme = Time.time;
            boutonTirRelache = Input.GetAxis("Fire1") > -0.2;
            mesArmes[currentWeapon].ActiveWeapon();

            model3DFPS[currentWeapon].SetActive(true);
            stockMuniTxt.text = nbMunitions.ToString();
            mesArmes[currentWeapon].MajInfoWeapon();
            isActive = true;
            playerName = myPlayer.pseudo;
            myPlayerAnimation.ChangerArme(1);
        }
        else
        {
            model3DExt[currentWeapon].SetActive(true);
        }

       
    }

    void Update()
    {
        if (!isLocalPlayer) return;
        if (myPlayer.frozen) return;
        if (!isActive) return;
        //Appui pour tirer
        if (Input.GetAxis("Fire1") > 0.2 || Input.GetButtonDown("Fire1"))
        {
            TirPrincipal();
            boutonTirRelache = false;
        }
        //relache la gachette ou le bouton 1 seule detection
        if ((Input.GetButtonUp("Fire1")  || (Input.GetAxis("Fire1") < 0.2 && myPlayer.controlManette)) && !boutonTirRelache)
        {
            RelacheTirPrincipal();
            boutonTirRelache = true;
        }

        if (Input.GetAxis("ZoomAxis") > 0.2 || Input.GetButton("ZoomButton"))
        {
            Zoomer(true);
            boutonZoomRelache = false;
        }
        if ((Input.GetButtonUp("ZoomButton") || (Input.GetAxis("ZoomAxis") < 0.2 && myPlayer.controlManette)) && !boutonZoomRelache)
        {
            Zoomer(false);
            boutonZoomRelache = true;
        }

        //Changement des armes
        if ( Input.GetAxis("Mouse ScrollWheel") > 0 && Time.time - timerChangementArme > freqChangementArme)
        {
            timerChangementArme = Time.time;
            UpWeaponSelection();

        }
        else if ( Input.GetAxis("Mouse ScrollWheel") < 0 && Time.time - timerChangementArme > freqChangementArme)
        {
            timerChangementArme = Time.time;
            DownWeaponSelection();

        }


        // MITRAILLEUSE
        if ((Input.GetAxis("ChangeWeaponYAxis") > 0 || Input.GetKey(KeyCode.Alpha1)) && Time.time - timerChangementArme > freqChangementArme)
        {
            timerChangementArme = Time.time;
            if(!mesArmes[(int)WeaponName.Mitrailleuse].CanBeUsed() && nbMunitions <= 0)
            {
                CmdSetWeapon(WeaponName.Pistolet);
            }
            else
            {
                CmdSetWeapon(WeaponName.Mitrailleuse);
            }

        }// FUSIL A POMPE
        else if ((Input.GetAxis("ChangeWeaponYAxis") < 0 || Input.GetKey(KeyCode.Alpha2)) && Time.time - timerChangementArme > freqChangementArme)
        {
            timerChangementArme = Time.time;
            if (!mesArmes[(int)WeaponName.FusilPompe].CanBeUsed() && nbMunitions <= 0)
            {
                CmdSetWeapon(WeaponName.Pistolet);
            }
            else
            {
                CmdSetWeapon(WeaponName.FusilPompe);
            }

        }// ONE SHOOTER
        else if ((Input.GetAxis("ChangeWeaponXAxis") > 0 || Input.GetKey(KeyCode.Alpha3)) && Time.time - timerChangementArme > freqChangementArme)
        {
            timerChangementArme = Time.time;
            if (!mesArmes[(int)WeaponName.OneShooter].CanBeUsed()
                && nbMunitions <= 0)
            {
                CmdSetWeapon(WeaponName.Pistolet);
            }
            else
            {
                CmdSetWeapon(WeaponName.OneShooter);
            }

        }//BAZOOKA
        else if ((Input.GetAxis("ChangeWeaponXAxis") < 0 || Input.GetKey(KeyCode.Alpha4)) && Time.time - timerChangementArme > freqChangementArme)
        {
            timerChangementArme = Time.time;
            if (!mesArmes[(int)WeaponName.Bazooka].CanBeUsed() && nbMunitions <= 0)
            {
                CmdSetWeapon(WeaponName.Pistolet);
            }
            else
            {
                CmdSetWeapon(WeaponName.Bazooka);
            }

        }else if (Input.GetButton("BackUpWeapon"))
        {
            CmdSetWeapon(WeaponName.Pistolet);
        }


        if (Input.GetButtonDown("Recharger"))
        {
            mesArmes[currentWeapon].TempoRecharger();
        }


    }

    //obtenir l'arme place en x
    [Command]
    public void CmdSetWeapon(WeaponName wn)
    {
            currentWeapon = (int)wn;
    }
    [Command]
    public void CmdSetWeapon2()
    {
        RpcPatchOnChangeWeapon();
    }

  

    //changement arme vers le haut
    public void UpWeaponSelection()
    {
        CmdCurrentWeapon(1);
    }

    [Command]
    void CmdCurrentWeapon(int decalage)
    {
        int i = currentWeapon;
        i+= decalage;
        if (i < 0) i = NB_WEAPONS - 1;
        if (i >= NB_WEAPONS) i = 0;
        currentWeapon = i;
    }

    //jouer le son de prise de pack
    [ClientRpc]
    public void RpcPlayGetMuniPack()
    {
        if (!isLocalPlayer) return;
        aS.PlayOneShot(sonPrisePackMuni, 0.7f);

    }

    //changement arme vers le bas
    public void DownWeaponSelection()
    {
        CmdCurrentWeapon(-1);
    }


    //tir principal de l'arme
    public void TirPrincipal()
    {
        mesArmes[currentWeapon].Tirer(myPlayer.pseudo);//(int)currentWeapon
    }

    //evenement de relache gachette
    public void RelacheTirPrincipal()
    {
        mesArmes[currentWeapon].RelacheTirer(myPlayer.pseudo);
    }

    public void Zoomer(bool order)
    {
        mesArmes[currentWeapon].Zooming(order);
    }


    //recharger l'arme en cours
    public void RechargeWeapon()
    {
        mesArmes[currentWeapon].TempoRecharger();
    }

    public void Destocker(int nbMun)
    {
        nbMunitions -= nbMun;
    }

    public void ResetWeaponsPack()
    {
        foreach(Weapon w in mesArmes)
        {
            w.ResetWeapon();
            w.MajInfoWeapon();
            CmdResetMunition();
        }
    }

    [Command]
    void CmdResetMunition()
    {
        nbMunitions = munitionsInitiales;
    }

    [Command]
    void CmdAddMuntion(int nbMuni)
    {
        nbMunitions += nbMuni;
   }

    public void AjouterMuntion(int nbMuni)
    {
        if (isLocalPlayer)
        {
            CmdAddMuntion(nbMuni);
        }
       
    }

    public void OnChangeMunition(int _nbMunitions)
    {
        this.nbMunitions = _nbMunitions;
        if (isLocalPlayer)
        {
            stockMuniTxt.text = nbMunitions.ToString();
            mesArmes[currentWeapon].MajInfoWeapon();
        }
            
    }

    public void OnChangeWeapon(int currentWeapon)
    {
        this.currentWeapon = currentWeapon;
        myPlayerAnimation.ChangerArme(currentWeapon);
        for (int i=0; i < NB_WEAPONS; i++)
        {
            mesArmes[i].DisableWeapon();
            if (isLocalPlayer)
            {
                model3DFPS[i].SetActive(false);
            }
            else
            {
                model3DExt[i].SetActive(false);
            }
        }

        mesArmes[currentWeapon].ActiveWeapon();
        if (isLocalPlayer)
        {
            model3DFPS[currentWeapon].SetActive(true);
            mesArmes[currentWeapon].MajInfoWeapon();
            if (OnChangeSelectedWeapon != null)
                OnChangeSelectedWeapon((WeaponName)currentWeapon);
            Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
        }
        else
        {
            model3DExt[currentWeapon].SetActive(true);
        }       
    }

    [ClientRpc]
    public void RpcPatchOnChangeWeapon()
    {
        for (int i = 0; i < NB_WEAPONS; i++)
        {
            mesArmes[i].DisableWeapon();
            if (isLocalPlayer)
            {
                model3DFPS[i].SetActive(false);
            }
            else
            {
                model3DExt[i].SetActive(false);
            }
        }

        mesArmes[currentWeapon].ActiveWeapon();
        if (isLocalPlayer)
        {
            model3DFPS[currentWeapon].SetActive(true);
            mesArmes[currentWeapon].MajInfoWeapon();
            if (OnChangeSelectedWeapon != null)
                OnChangeSelectedWeapon((WeaponName)currentWeapon);
            Utils.Import.XInputGamePadSetState(0, 0.0f, 0.0f);
        }
        else
        {
            model3DExt[currentWeapon].SetActive(true);
        }
    }


    //lors de la mort du joueur on fait disparaire les models
    public void DesactivateWeaponModel()
    {
        for (int i = 0; i < NB_WEAPONS; i++)
        {
            mesArmes[i].DisableWeapon();
            if (isLocalPlayer)
            {
                model3DFPS[i].SetActive(false);
            }
            else
            {
                model3DExt[i].SetActive(false);
            }
            
        }
        isActive = false;
    }

    public void ReactivateWeaponModel()
    {
        isActive = true;
        if (currentWeapon != 1)
            CmdSetWeapon(WeaponName.Mitrailleuse);
        else CmdSetWeapon2();  
    }

    // retourne l'intensité de l'agilité de l'arme en cours
    public float GetWeaponAgilityMovement()
    {
        if (!boutonZoomRelache)
        { // si il zoome
            return mesArmes[currentWeapon].zoomingAgiliteMouvement;
        }
        return mesArmes[currentWeapon].normalAgiliteMouvement;
    }  
}
