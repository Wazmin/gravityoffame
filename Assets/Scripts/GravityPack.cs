﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class GravityPack : NetworkBehaviour {
    private bool[] isMajKeyDown = new bool [2];
    private bool[] isZKeyDown = new bool[2];
    private bool goFrontWall;

    public PlayerAnimations myPlayerAnimation;
    public ShipMovement myShipMovement;
    public Player myPlayer;
    public float intensiteGravite;
    public float saut;
    public bool grounded;
    public bool canDoubleJump = true;
    public float doubleJumpTimer;
    public bool isChangingGravity;
    float angleToTurn;
    public float degreeToTurn = 5;
    private char keyChangeGravity;
    private int numChangeGravity;

    private bool controlManette;
    // Ref Transform exterieur
    private Transform tCamera;

    //Timer pour controle touche
    private float keyTimer;
    public float freqKeyTimer = 0.5f;
    private float rotationTimer;
    public float freqRotationTimer = 0.2f;
    private Rigidbody joueur;

    // PARTIE AUDIO
    AudioSource aS;
    public AudioClip sonSautImpulsion;
    public AudioClip sonSautReception;
    public AudioClip sonDoubleSaut;

    public AudioClip sonPublic1;
    public AudioClip sonPublic2;
    // directional vector gravity the player want to change
    private Vector3 vDirectionChangeGravity;
    //directional vector for the rotation of the player body
    private Vector3 vRotationPlayer;
    private Vector3 vRotationCamera;
    // lookAt vector
    private Vector3 playerLookAt;

    /** GRAVITY **/
    // World Gravity References Vectors
    struct GravityVector{
        public string name;
        public Vector3 vector;

        public GravityVector(string s, Vector3 v)
        {
            name = s;
            vector = v;
        }
    }
    GravityVector[] tabGravityVector = new GravityVector[6];
    // Gravity vector tu use
    private Vector3 gravity;

    private StatsTest StatPlayer;

    // Use this for initialization
    public override void OnStartLocalPlayer()
    {
        Cursor.visible = false;
        keyTimer = Time.time;
        rotationTimer = keyTimer;
        doubleJumpTimer = keyTimer;
        grounded = false;
        myPlayerAnimation.SetIsGrounded(grounded);
        joueur = this.GetComponent<Rigidbody>();
        isChangingGravity = false;
        angleToTurn = 0;
        keyChangeGravity = 'X';
        tCamera = transform.Find("PlayerCamera").transform;
        // init du tableau des vecteurs gravité
        tabGravityVector[0] = new GravityVector("RIGHT_WORLD", Vector3.right);
        tabGravityVector[1] = new GravityVector("LEFT_WORLD", Vector3.left);
        tabGravityVector[2] = new GravityVector("UP_WORLD", Vector3.up);
        tabGravityVector[3] = new GravityVector("DOWN_WORLD", Vector3.down);
        tabGravityVector[4] = new GravityVector("FORWARD_WORLD", Vector3.forward);
        tabGravityVector[5] = new GravityVector("BACK_WORLD", Vector3.back);

        // catch the player's directionnals points
        gravity = tabGravityVector[3].vector;

        aS = GetComponent<AudioSource>();

        //Link de la classe stats
        StatPlayer = GetComponent<StatsTest>();
        numChangeGravity = -1;
    }

	
	// Update is called once per frame
	void Update () {
        if (!isLocalPlayer) return;
        if (myPlayer.frozen) return;
        if (myPlayer.isDead) return;
        clampAngleCam();

    }

    void FixedUpdate()
    {
        if (!isLocalPlayer) return;
        if (myPlayer.frozen) return;
        if (myPlayer.isDead && !isChangingGravity) return;

        // garde fou gravité avant
        // touche Maj
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isMajKeyDown[1] = true;
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isMajKeyDown[1] = false;
        }
        // touche Z
        if (Input.GetKeyDown(KeyCode.Z))
        {
            isZKeyDown[1] = true;
        }
        else if (Input.GetKeyUp(KeyCode.Z))
        {
            isZKeyDown[1] = false;
        }

        if(isZKeyDown[1] && isMajKeyDown[0] && !isZKeyDown[0] )
        {
            goFrontWall = true;
        }
        else
        {
            goFrontWall = false;
        }


        //changeGravity();
        if (isChangingGravity)
        {
            rotatePlayer();
        }
        else
        {
            // gravity (of Fame)
            joueur.AddForce(gravity * intensiteGravite, ForceMode.Force);
            changeGravity();

        }
        if (grounded && canDoubleJump)
        {
            if ((Input.GetKey(KeyCode.Space) && !Input.GetKey(KeyCode.LeftShift)) || Input.GetButtonDown("Jump"))
            {
                aS.PlayOneShot(sonSautImpulsion);
                joueur.AddForce(-gravity * saut, ForceMode.VelocityChange);
                myPlayerAnimation.SetJump();
                grounded = false;
                myPlayerAnimation.SetIsGrounded(false);
                doubleJumpTimer = Time.time;
                StatPlayer.nbSaut++;
            }
        }
        else if (!grounded && canDoubleJump && (Time.time - doubleJumpTimer) > 0.2f)
        {

            if ((Input.GetKey(KeyCode.Space) && !Input.GetKey(KeyCode.LeftShift)) || Input.GetButton("Jump"))
            {
                aS.PlayOneShot(sonDoubleSaut);
                joueur.AddForce(-gravity * saut, ForceMode.VelocityChange);
                if (joueur.velocity.magnitude > 4.0f)
                {
                    joueur.velocity = joueur.velocity.normalized * 2;
                }
                canDoubleJump = false;
                StatPlayer.nbDoubleSaut++;

            }
        }

        isMajKeyDown[0] = isMajKeyDown[1];
        isZKeyDown[0] = isZKeyDown[1];


    }

    // find the vector tu use as gravity
    // depend on the player position
    void changeGravity()
    {
        // key Shift needed to be bind with Z,Q,S,D
        if ( (myPlayer.controlManette || Input.GetKey(KeyCode.LeftShift)) && !isChangingGravity && (Time.time - keyTimer) > freqKeyTimer)
        {
           
            if (Input.GetKey(KeyCode.D) || Input.GetButton("RightWall"))
            {
                vDirectionChangeGravity = transform.right;
                vDirectionChangeGravity.Normalize();
                vRotationPlayer = transform.forward;
                vRotationPlayer.Normalize();
                keyChangeGravity = 'D';
                vRotationCamera = transform.Find("PlayerCamera").transform.forward;
                //reset timer if gravity changed
                if (changeGravityVector())
                {
                    keyTimer = Time.time;
                    grounded = false;
                    rotatePlayer();
                    myPlayerAnimation.SetIsGrounded(grounded);
                    myPlayerAnimation.SetChangeGravity(1);
                    StatPlayer.nbChangementGravite++;
                    StatPlayer.nbChangementGraviteRight++;
                }
            }else if (Input.GetKey(KeyCode.Q) || Input.GetButton("LeftWall"))
            {
                vDirectionChangeGravity = -transform.right;
                vDirectionChangeGravity.Normalize();
                vRotationPlayer = -transform.forward;
                vRotationPlayer.Normalize();
                keyChangeGravity = 'Q';
                vRotationCamera = -transform.Find("PlayerCamera").transform.forward;
                //reset timer if gravity changed
                if (changeGravityVector())
                {
                    keyTimer = Time.time;
                    grounded = false;
                    rotatePlayer();
                    myPlayerAnimation.SetIsGrounded(grounded);
                    myPlayerAnimation.SetChangeGravity(2);
                    StatPlayer.nbChangementGravite++;
                    StatPlayer.nbChangementGraviteLeft++;
                }
            }else if (goFrontWall || Input.GetButton("FrontWall"))
            {
                vDirectionChangeGravity = transform.forward;
                vDirectionChangeGravity.Normalize();
                vRotationPlayer = -transform.right;
                vRotationPlayer.Normalize();
                keyChangeGravity = 'Z';
                //reset timer if gravity changed
                if (changeGravityVector())
                {
                    keyTimer = Time.time;
                    grounded = false;
                    rotatePlayer();
                    myPlayerAnimation.SetIsGrounded(grounded);
                    myPlayerAnimation.SetChangeGravity(3);
                    StatPlayer.nbChangementGravite++;
                    StatPlayer.nbChangementGraviteDevant++;
                }
            }else if (Input.GetKey(KeyCode.S) || Input.GetButton("BackWall"))
            {
                vDirectionChangeGravity = -transform.forward;
                vDirectionChangeGravity.Normalize();
                vRotationPlayer = transform.right;
                vRotationPlayer.Normalize();
                keyChangeGravity = 'S';
                //reset timer if gravity changed
                if (changeGravityVector())
                {
                    keyTimer = Time.time;
                    grounded = false;
                    rotatePlayer();
                    myPlayerAnimation.SetIsGrounded(grounded);
                    myPlayerAnimation.SetChangeGravity(4);
                    StatPlayer.nbChangementGravite++;
                    StatPlayer.nbChangementGraviteArriere++;
                }
            }

            if (Input.GetKey(KeyCode.Y))
            {
                Debug.Log(Vector3.Angle(transform.right, transform.Find("PlayerCamera").transform.right));

                keyTimer = Time.time;

            }
        }
    }

    // return if the gravity vector changed
    // the order of the line matter
    private bool changeGravityVector()
    {
        int iToGravity = -1;
        int iToRotation = -1;
        float tmp = Mathf.PI;
        float tmp2 = tmp;
        bool changed = false;


            // trouve le plus petit angle pour la gravité
            for (int i = 0; i < 6; i++)
            {
                if (angleGeometrique(vDirectionChangeGravity, tabGravityVector[i].vector) < tmp)
                {
                    iToGravity = i;
                    tmp = angleGeometrique(vDirectionChangeGravity, tabGravityVector[i].vector);
                }
                if (angleGeometrique(vRotationPlayer, tabGravityVector[i].vector) < tmp2)
                {
                    iToRotation = i;
                    tmp2 = angleGeometrique(vRotationPlayer, tabGravityVector[i].vector);
                }
            }
            vRotationPlayer = tabGravityVector[iToRotation].vector;
            changed = tabGravityVector[iToGravity].vector != gravity;
            isChangingGravity = changed;
            gravity = tabGravityVector[iToGravity].vector;
  
        return changed;
    }

    void rotatePlayer()
    {
        if (angleToTurn <=0) {
            RaycastHit hit;
            Ray myRay;
            Vector3 whereToMove;
            //myShipMovement.StopMovement();
            if (keyChangeGravity == 'D')
            {
                myRay= new Ray(transform.position, transform.right);
                whereToMove = -transform.right;
            }
            else if (keyChangeGravity == 'Q')
            {
                myRay = new Ray(transform.position, -transform.right);
                whereToMove = transform.right;
            }
            else if (keyChangeGravity == 'Z')
            {
                myRay = new Ray(transform.position, transform.forward);
                whereToMove = -transform.forward;
            }
            else if (keyChangeGravity == 'S')
            {
                myRay = new Ray(transform.position, -transform.forward);
                whereToMove = transform.forward;
            }
            else
            {
                myRay = new Ray(transform.position, -transform.forward);
                whereToMove = -transform.forward;
            }

            int layer = 1 << 11;
            if (Physics.Raycast(myRay, out hit, 1.5f,layer))
            {

                if (hit.collider.tag == "WallMap")
                {
                    transform.position += (whereToMove * (1.6f -hit.distance )); 
                }

            }
        }

        if ((Time.time - rotationTimer) > freqRotationTimer)
        {
            rotationTimer = Time.time;
            angleToTurn += degreeToTurn;
            if (keyChangeGravity == 'Q' )
            {
                // rotation camera
                transform.RotateAround(transform.Find("PlayerCamera").transform.position, vRotationCamera, degreeToTurn);
                if (angleToTurn>=90)
                {
                    float angleT = Vector3.Angle(-transform.up, gravity);
                    if(angleT != 0)
                    {
                        Vector3 vT = Vector3.Cross(-transform.up, gravity);
                        Vector3 lK = transform.Find("PlayerCamera").transform.forward;
                        Vector3 pT = transform.Find("PlayerCamera").transform.position + 10000 * lK;
                        Quaternion rotation = Quaternion.LookRotation(lK, -gravity);

                        //place le body pour etre a plat sur le nouveau sol
                        transform.RotateAround(transform.Find("PlayerCamera").transform.position, vT, angleT);

                        //deplace la camera pour le tracking cible
                        transform.Find("PlayerCamera").transform.LookAt(pT, transform.up);

                        //calcul la différence entre les axes body et cam selon l'axe UP (-gravity) 
                        angleT = Vector3.Angle(transform.right, transform.Find("PlayerCamera").transform.right);

                        if (Vector3.Angle(transform.forward, transform.Find("PlayerCamera").transform.right) > 90)
                        {
                            transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, angleT);
                            transform.Find("PlayerCamera").transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, -angleT);
                        }
                        else
                        {
                            transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, -angleT);
                            transform.Find("PlayerCamera").transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, angleT);
                        }
                    }

                }
            
            }
            else if (keyChangeGravity == 'D')
            {
                // rotation camera
                transform.RotateAround(transform.Find("PlayerCamera").transform.position, vRotationCamera, degreeToTurn);
                if (angleToTurn >= 90)
                {
                    float angleT = Vector3.Angle(-transform.up, gravity);
                    if(angleT != 0)
                    {
                        Vector3 vT = Vector3.Cross(-transform.up, gravity);
                        Vector3 lK = transform.Find("PlayerCamera").transform.forward;
                        Vector3 pT = transform.Find("PlayerCamera").transform.position + 10000 * lK;
                        //place le body pour etre a plat sur le nouveau sol
                        transform.RotateAround(transform.Find("PlayerCamera").transform.position, vT, angleT);

                        //deplace la camera pour le tracking cible
                        transform.Find("PlayerCamera").transform.LookAt(pT, transform.up);

                        //calcul la différence entre les axes body et cam selon l'axe UP (-gravity) 
                        angleT = Vector3.Angle(transform.right, transform.Find("PlayerCamera").transform.right);

                        if (Vector3.Angle(transform.forward, transform.Find("PlayerCamera").transform.right) > 90)
                        {
                            transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, angleT);
                            transform.Find("PlayerCamera").transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, -angleT);
                        }
                        else
                        {
                            transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, -angleT);
                            transform.Find("PlayerCamera").transform.RotateAround(transform.Find("PlayerCamera").transform.position, -gravity, angleT);
                        }
                    }
                }
            }
            else if (keyChangeGravity == 'Z')
            {
                // rotation camera
                transform.RotateAround(transform.Find("PlayerCamera").transform.position, vRotationPlayer, degreeToTurn);
                transform.Find("PlayerCamera").transform.RotateAround(transform.Find("PlayerCamera").transform.position, transform.right, degreeToTurn);

            }
            else
            {
                // rotation camera
                transform.RotateAround(transform.Find("PlayerCamera").transform.position, vRotationPlayer, degreeToTurn);
                transform.Find("PlayerCamera").transform.RotateAround(transform.Find("PlayerCamera").transform.position, -transform.right, degreeToTurn);

            }

            if (angleToTurn >= 90.0f)
            {
                isChangingGravity = false;
                myPlayerAnimation.SetChangeGravity(-1);
                angleToTurn = 0;
            }


        }

    }

    // clamp camera angle
    private void clampAngleCam()
    {
        

        float angleCam = Vector3.Angle(transform.forward, tCamera.forward);
        float angleTopCam = Vector3.Angle(transform.up, tCamera.forward);

        if ( angleCam > 70.0f)
        {
            if (angleTopCam>90)
            {
                tCamera.RotateAround(tCamera.position, tCamera.right, 70.0f - angleCam);
            }
            else
            {
                tCamera.RotateAround(tCamera.position, tCamera.right, angleCam- 70.0f);
            }
        }
    }

    // calculation of the geometric angle in radian
    public float angleGeometrique(Vector3 vPlayer, Vector3 vDirection)
    {
        return (Mathf.Acos((Vector3.Dot(vPlayer, vDirection))));
    }

    // access to the gravity value
    /* public void activateGravityPack()
     {
         float x, y, z;

         if (gravity == tabGravityVector[0].vector)
         {
             //x = transform.rotation.x;
             //y = transform.rotation.y;
             transform.Rotate(Vector3.forward, 90);
         }
         if (gravity == tabGravityVector[3].vector)
         {
             //x = transform.rotation.x;
             //y = transform.rotation.y;
             transform.Rotate(Vector3.forward, 90);
         }
     }*/

    //convertion degree radian
    public float DegreeToRadian(float angle)
    {
        return Mathf.PI * angle / 180.0f;
    }

    public float RadianToDegree(float angle)
    {
        return 180 * angle / Mathf.PI;
    }

}


/*
Debug.Log("DIRECTION PLAYER" + vDirectionChangeGravity);
Debug.Log("RIGHT" + Vector3.Cross(vDirectionChangeGravity, RIGHT_WORLD));
Debug.Log("RIGHT ANGLE" + angleGeometrique(vDirectionChangeGravity, RIGHT_WORLD));
Debug.Log("LEFT" + Vector3.Cross(vDirectionChangeGravity, LEFT_WORLD));
Debug.Log("LEFT ANGLE" + angleGeometrique(vDirectionChangeGravity, LEFT_WORLD));
Debug.Log("UP" + Vector3.Cross(vDirectionChangeGravity, UP_WORLD));
Debug.Log("UP ANGLE" + angleGeometrique(vDirectionChangeGravity, UP_WORLD));
Debug.Log("DOWN" + Vector3.Cross(vDirectionChangeGravity, DOWN_WORLD));
Debug.Log("DOWN ANGLE" + angleGeometrique(vDirectionChangeGravity, DOWN_WORLD));
Debug.Log("FOWARD" + Vector3.Cross(vDirectionChangeGravity, FORWARD_WORLD));
Debug.Log("FOWARD ANGLE" + angleGeometrique(vDirectionChangeGravity, FORWARD_WORLD));
Debug.Log("BACK" + Vector3.Cross(vDirectionChangeGravity, BACK_WORLD));
Debug.Log("BACK ANGLE" + angleGeometrique(vDirectionChangeGravity, BACK_WORLD));
*/
